package iphone;

import com.google.gson.*;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import model.Model;
import model.Player;
import model.GameEngine;
import view.GameListener;

public class IOSGameViewProxy implements GameListener {

    private static boolean quit = false;
    private static PrintStream out = null;
    private static BufferedReader in = null;
    Model engine;
    Player player;

    // Constructor
    public IOSGameViewProxy(Model engine, Player p) {
        this.engine = engine;
        this.player = p;
        engine.addGameListener(this);
        ServerSocket connectionServer = null;
        Socket clientSession = null;
        int port = 4444;

        //  final int time = 10;
        final ServiceAnnouncer service = new ServiceAnnouncer();

        // Register service on that server socket
        System.out.println("Registering service...");
        service.registerService(port);

        while (!quit) {
            try {
                System.out.println("\nNow accepting connections...");
                connectionServer = new ServerSocket(4444);
                clientSession = connectionServer.accept();
                System.out.println("Connection was accepted");
                out = new PrintStream(clientSession.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(clientSession.getInputStream()));

                System.out.println("Initial room is " + player.getCurrentRoom().getDescription() + " with exits: " + code(player.getCurrentRoom().getExits()));
                //out.println(player.getCurrentRoom().getDescription() + "/" + code(player.getCurrentRoom().getExits()) + "/" + player.getCurrentRoom().getImageName());
              
                HashMap gameInfos = getGameInformations();
                
                Gson gson = new Gson();
                String jsonString = gson.toJson(gameInfos);
                out.println(jsonString);
                
                String command;
                while ((command = in.readLine()) != null) {
                    System.out.println("Received command " + command);
                    engine.interpretCommand(command);
                    
                }

                System.out.println("Reading loop ended");
                out.close();
                in.close();
                clientSession.close();
                connectionServer.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }
/*
    @Override
    public void run()
    {
        
        
    }
    */
    public void gameStateModified(String roomName) {
        if (out != null) {
            System.out.println("Initial room is " + player.getCurrentRoom().getDescription() + " with exits: " + code(player.getCurrentRoom().getExits()) + " " + player.getCurrentRoom().getImageName());
            //out.println(player.getCurrentRoom().getDescription() + "/" + code(player.getCurrentRoom().getExits()) + "/" + player.getCurrentRoom().getImageName());
            HashMap gameInfos = getGameInformations();
                
            Gson gson = new Gson();
            String jsonString = gson.toJson(gameInfos);
                //System.out.println(jsonString);
             out.println(jsonString);
        }
    }

    private String code(HashMap exits) {
        @SuppressWarnings("unchecked")
        Set<String> set = exits.keySet();
        String code = "";
        for (String s : set) {
            code = code + s + ",";
        }
        return code;
    }
    
    private String base64EncodedImage(String imageName)
    {
        try {
        
            Path path = null;
        
            //path = Paths.get(getClass().getResource(imageName).toURI());
            
            BufferedImage origImage = ImageIO.read(getClass().getResource(imageName));
            
            BufferedImage resizedImage = resize(origImage, 280, 200);
            
            File tmpFile = File.createTempFile("roomImage", ".jpg");
            ImageIO.write(resizedImage, "jpg", tmpFile);
            
            path = Paths.get(tmpFile.toURI());
            
            byte[] imageData = null;
            
            imageData = Files.readAllBytes(path);
        
            Encoder enc = Base64.getEncoder();
        
            return enc.encodeToString(imageData);
        
        } catch (IOException ex ) {
            Logger.getLogger(IOSGameViewProxy.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static BufferedImage resize(BufferedImage image, int width, int height)
    {
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
        Graphics2D g2d = (Graphics2D) bi.createGraphics();
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        g2d.drawImage(image, 0, 0, width, height, null);
        g2d.dispose();
        return bi;
    }
    
    private HashMap getGameInformations()
    {
        HashMap<String, Object> map = new HashMap<>();
        map.put("roomDescription", player.getCurrentRoom().getDescription());
        map.put("roomExits", player.getCurrentRoom().getExits().keySet());
        map.put("roomName", player.getCurrentRoom().getImageName());
        map.put("gold", player.getGold());
        map.put("life", player.getLife());
        map.put("itemWeight", player.getCurrentWeight());
        map.put("roomsItems", player.getCurrentRoom().getItems());
        map.put("inventoryItems", player.getItems());
        map.put("characters", player.getCurrentRoom().getCharacters());
        
        GameEngine eg = (GameEngine)engine;
        String output = eg.getOutputString();
        
        if (output != null) {
            map.put("output", output);
        }
        
/*
        String b64roomImage = base64EncodedImage(player.getCurrentRoom().getImageName());
        if (b64roomImage == null) {
            return null;
        }

        map.put("roomImage", b64roomImage);
        */

        return map;
    }
}
