/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package zuul;

import java.awt.Dimension;
import java.net.URL;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import model.Game;
import view.MainMenuPanel;
/**
 *
 * @author 11405933
 */
public class ZuulStart {
    
    public JFrame myFrame;
    private JLayeredPane mainMenuPanel;
    private JButton histLabel;
    private int i;
    private String playername;
    
    public ZuulStart(){
        initGUI();
}
    private void initGUI(){
        myFrame = new JFrame("THE WORLD OF ZUUL");
        myFrame.setPreferredSize(new Dimension(965, 725));
        myFrame.setResizable(false);
        mainMenuPanel = new MainMenuPanel(this);   
        myFrame.getContentPane().add(mainMenuPanel);
        myFrame.setLocationRelativeTo(null);
        myFrame.pack();
        myFrame.setVisible(true);
        
        // to intialize javafx for mp3
        JFXPanel fxPanel = new JFXPanel();
        
    }
    public void startGameGUI (String s) {
        playername = s;
        histLabel = new JButton();
        histLabel.setSize(new Dimension(965, 725));
        histLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/hist1.jpg")));
        i = 1;
        histLabel.setLocation(0,0);
        histLabel.setContentAreaFilled(false);
        histLabel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        histLabel.setFocusPainted(false);
        histLabel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeNextImage();
            }
        });
        myFrame.remove(mainMenuPanel);
        myFrame.add(histLabel);
        myFrame.pack();
    }
    public void loadgame(String playerName){
        myFrame.remove(mainMenuPanel);
        Game myGame = new Game(myFrame, playerName, true);        
        myFrame.pack();
    }
    private void changeNextImage(){
        if(i<4){
            i += 1;
            histLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/hist"+i+".jpg")));
        }
        else{
            myFrame.remove(histLabel);
            Game myGame = new Game(myFrame, playername, false);        
            myFrame.pack();
        }
    }
    public void exit(){
        System.exit(0);
    }
}
