/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.awt.Point;
import java.io.Serializable;

/**
 *
 * @author djayb6
 */
public class Item implements Serializable {
    
    private String name;
    private String description;
    private String imagePath;
    private Point position;
    private int weight;
    private int price;
    private boolean canBeTaken;
   
    public Item(String name, String description, int weight, int price, boolean canBeTaken, String imagePath, Point position) {
        this.name = name;
        this.description = description;
        this.weight = weight;
        this.price = price;
        this.canBeTaken = canBeTaken;
        this.imagePath = imagePath;
        this.position = position;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean canBeTaken() {
        return canBeTaken;
    }

    public void setCanBeTaken(boolean canBeTaken) {
        this.canBeTaken = canBeTaken;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }
    
    public void setPrice(int price)
    {
        this.price = price;
    }
    
    public int getPrice()
    {
        return price;
    }
    
    
    
}
