package model;

import communication.GameViewProxy;
import controller.Parser;
import iphone.IOSGameViewProxy;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javax.swing.JFrame;
import model.RoomDirections;
import view.GameListener;
import view.GameView;

/**
 * This class is the main class of the "World of Zuul" application. "World of
 * Zuul" is a very simple, text based adventure game. Users can walk around some
 * scenery. That's all. It should really be extended to make it more
 * interesting!
 *
 * To play this game, create an instance of this class and call the "play"
 * method.
 *
 * This main class creates and initializes all the others: it creates all rooms,
 * creates the parser and starts the game.
 *
 * Based on Michael Kolling's and David J. Barnes' original source codes.
 */
public class Game {

    private Parser parser;
    private Player player;
    private GameEngine engine;

    // Create the Game and initialize its internal map.     
    public Game(JFrame myframe, String name, Boolean load) {
               
        player = new Player(name);
        parser = new Parser();
        engine = new GameEngine(parser, player);
        if(load){
            engine.interpretCommand("load");
        }
        else{
            setFirstOutput();
            createRooms();
        }

        // GUI must be created last since it needs all above classes instances (engine, player, rooms) to display game.
        GameListener localView = new GameView(engine, myframe);


//******************* CODE FOR IPHONE PART OF THE PROJECT ***********************************      
//******************* opens sockets for possible remote Java views      

        GameListener remoteJavaView = new GameViewProxy(engine);
        // calls the run method of GameViewProxy
        ((Thread) remoteJavaView).start(); 
        //opens sockets for possible remote iOS views
        
        new Thread(() -> {
            GameListener remoteiOSView = new IOSGameViewProxy(engine, player);
        }).start();
        //((Thread) remoteiOSView).start();
       
        /*
        new Thread(() -> {
        
        String bip = "/sound/zuul.mp3";
        URL url = getClass().getResource(bip);
        System.out.println(url);
        Media hit = new Media(url.toString());
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.setAutoPlay(true);
        mediaPlayer.play();
        }).start();
                */
//*******************************************************************************************
    }

    // Create all the rooms and link their exits together.
    private void createRooms() {
        
        Room forest, hutYard, hut, cavern, mountain, magicRoom, waterMill, pontoon,
                river, castleShore, lobby, banquetHall, arsenal, office, pub, stairsRoom,
                sewers, prison, throneRoom;
        
        forest = new Room(RoomName.FOREST, "a peaceful forest", "/images/forest.jpg");
        hutYard = new Room(RoomName.HUT_YARD, "the yard of a hut", "/images/hut_yard.jpg");
        hut = new Room(RoomName.HUT, "a hut with some interesting stuff inside", "/images/hut.jpg");
        cavern = new Room(RoomName.CAVERN, "a dark cavern", "/images/cavern.jpg");
        mountain = new Room(RoomName.MOUNTAIN, "some hostile mountains", "/images/mountain.jpg");
        magicRoom = new Room(RoomName.MAGIC_ROOM, "a magic room with magical events", "/images/magic_room.jpg");
        waterMill = new Room(RoomName.WATER_MILL, "a watermill along the river", "/images/watermill.jpg");
        pontoon = new Room(RoomName.PONTOON, "a pontoon to board a boat", "/images/pontoon.jpg");
        river = new Room(RoomName.RIVER, "a whitewaters river", "/images/river.jpg");
        castleShore = new Room(RoomName.CASTLE_SHORE, "the shores of the castel", "/images/castle_shore.jpg");
        lobby = new Room(RoomName.LOBBY, "the lobby of the castel", "/images/lobby.jpg");
        banquetHall = new Room(RoomName.BANQUET_HALL, "the banquet hall with plenty of food inside", "/images/banquet_hall.jpg");
        arsenal = new Room(RoomName.ARSENAL, "an armory filled of dangerous weapons", "/images/armory.jpg");
        office = new Room(RoomName.OFFICE, "a messy office", "/images/office.jpg");
        pub = new Room(RoomName.PUB, "a pub with cheap drinks", "/images/pub.jpg");
        stairsRoom = new Room(RoomName.STAIRS_ROOM, "a room with stairs", "/images/stairs_room.jpg");
        sewers = new Room(RoomName.SEWERS, "some stinky sewers", "/images/sewers.jpg");
        prison = new Room(RoomName.PRISON, "the prison with dangerous prisoners", "/images/prison.jpg");
        throneRoom = new Room(RoomName.THRONE_ROOM, "the throne room with a menacing dragon", "/images/throne_room.jpg");
        
        // set exits to rooms
        
        hut.setExit(RoomDirections.SOUTH, hutYard);
        
        hutYard.setExit(RoomDirections.NORTH, hut);
        hutYard.setExit(RoomDirections.WEST, forest);
        
        forest.setExit(RoomDirections.NORTH, cavern);
        forest.setExit(RoomDirections.SOUTH, pontoon);
        forest.setExit(RoomDirections.EAST, hutYard);
        forest.setExit(RoomDirections.WEST, magicRoom);
        
        cavern.setExit(RoomDirections.SOUTH, forest);
        cavern.setExit(RoomDirections.WEST, mountain);
        
        mountain.setExit(RoomDirections.SOUTH, magicRoom);
        mountain.setExit(RoomDirections.EAST, cavern);
        
        magicRoom.setExit(RoomDirections.NORTH, mountain);
        magicRoom.setExit(RoomDirections.SOUTH, waterMill);
        magicRoom.setExit(RoomDirections.EAST, forest);
        
        waterMill.setExit(RoomDirections.NORTH, magicRoom);
        waterMill.setExit(RoomDirections.EAST, pontoon);
        
        pontoon.setExit(RoomDirections.NORTH, forest);
        pontoon.setExit(RoomDirections.EAST, river);
        pontoon.setExit(RoomDirections.WEST, waterMill);
        
        river.setExit(RoomDirections.EAST, castleShore);
        river.setExit(RoomDirections.WEST, pontoon);
        
        castleShore.setExit(RoomDirections.NORTH, lobby);
        castleShore.setExit(RoomDirections.WEST, river);
        
        lobby.setExit(RoomDirections.NORTH, banquetHall);
        lobby.setExit(RoomDirections.SOUTH, castleShore);
        
        banquetHall.setExit(RoomDirections.NORTH, arsenal);
        banquetHall.setExit(RoomDirections.SOUTH, lobby);
        banquetHall.setExit(RoomDirections.EAST, stairsRoom);
        banquetHall.setExit(RoomDirections.WEST, pub);
        
        arsenal.setExit(RoomDirections.SOUTH, banquetHall);
        arsenal.setExit(RoomDirections.WEST, office);
        
        office.setExit(RoomDirections.SOUTH, pub);
        office.setExit(RoomDirections.EAST, banquetHall);
        
        pub.setExit(RoomDirections.NORTH, office);
        pub.setExit(RoomDirections.EAST, banquetHall);
        
        stairsRoom.setExit(RoomDirections.NORTH, throneRoom);
        stairsRoom.setExit(RoomDirections.SOUTH, sewers);
        stairsRoom.setExit(RoomDirections.WEST, banquetHall);
        
        sewers.setExit(RoomDirections.NORTH, stairsRoom);
        sewers.setExit(RoomDirections.EAST, prison);
        
        prison.setExit(RoomDirections.WEST, sewers);
        
        throneRoom.setExit(RoomDirections.SOUTH, stairsRoom);
        
        // if locked, player can access the room under conditions
        
        hut.lock();
        pontoon.lock();
        river.lock();
        castleShore.lock();
        banquetHall.lock();
        prison.lock();
        throneRoom.lock();
        
        
        // add items to rooms
        
        Item wood, rope, rock, key, map, bag, lantern, flute, bearskin, greenDoor, redDoor, brownDoor, crown, raft;
        LifeItem bread, wine, cheese, fruit, potion, beer, gourd;
        Weapon slingshot, knife, sword, axe, arbalet;
        
        wood = new Item(ItemName.WOOD, "some wood to build useful things", 10000, -1, false, "/images/wood.png", null);
        rope = new Item(ItemName.ROPE, "some rope", 1000, -1, true, "/images/rope.png", null);
        //raft = new Item(ItemName.RAFT, "a solid raft", 11000, -1, true, null, null);
        rock = new Item(ItemName.ROCK, "a heavy rock which can break things", 10000, -1, true, "/images/rock.png", null);
        key = new Item(ItemName.KEY, "a golden key", 500, -1, false, "/images/key.png", null);
        map = new Item(ItemName.MAP, "a map of the world", 100, -1, false, null, null);
        bag = new Item(ItemName.BAG, "a bag to carry other items", 800, -1, true, "/images/bag.png", null);
        lantern = new Item(ItemName.LANTERN, "a lantern to enlighten darkness", 1000, -1, false, "/images/lantern.png", null);
        flute = new Item(ItemName.FLUTE, "a flute to play lullabies", 600, -1, false, "/images/flute.png", null);
        bearskin = new Item(ItemName.BEARSKIN, "a magical bearskin", 1000, -1, true, "/images/bearskin.png", null);
        greenDoor = new Item(ItemName.GREEN_DOOR, "a magical green door", 0, -1, false, null, null);
        redDoor = new Item(ItemName.RED_DOOR, "a magical red door", 0, -1, false, null, null);
        brownDoor = new Item(ItemName.BROWN_DOOR, "a magical brown door", 0, -1, false, null, null);
        crown = new Item(ItemName.CROWN, "the crown", 0, -1, true, "/images/crown.png", null);
        
        bread = new LifeItem(ItemName.BREAD, "some bread", 200, -1, true, "/images/bread.png", null, 30);
        wine = new LifeItem(ItemName.WINE, "some fruity French wine", 500, -1, true, "/images/wine.png", null, 40);
        cheese = new LifeItem(ItemName.CHEESE, "some gruyere cheese", 400, -1, true, "/images/cheese.png", null, 35);
        fruit = new LifeItem(ItemName.FRUIT, "a juicy fruit", 300, -1, false, "/images/fruit.png", null, 50);
        potion = new LifeItem(ItemName.POTION, "a healthy potion", 600, 300, true, "/images/potion.png", null, 90);
        beer = new LifeItem(ItemName.BEER, "a fresh beer", 500, 100, true, "/images/beer.png", null, 50);
        gourd = new LifeItem(ItemName.GOURD, "a gourd of stagnant water", 900, -1, true, "/images/gourd.png", null, 30);
        
        slingshot = new Weapon(ItemName.SLINGSHOT, "a wooden slingshot", 500, -1, true, "/images/slingshot.png", null, 10);
        knife = new Weapon(ItemName.KNIFE, "a keen knife", 500, -1, true, "/images/knife.png", null, 30);
        axe = new Weapon(ItemName.AXE, "a rusty axe", 1500, -1, true, null, null, 50);
        arbalet = new Weapon(ItemName.ARBALET, "a hunting arbalet", 1500, -1, false, "/images/arbalet.png", null, 55);
        sword = new Weapon(ItemName.SWORD, "a poisoned sword", 2000, -1, false, "/images/sword.png", null, 70);
                
        // characters
        
        Enemy bear, blackKnight, aligator, tarentula, dragon, gateKeeper, fantom, gobelin;
        Friend gandalf, prisoner;
        Merchant bartender;
        
        bear = new Enemy(CharacterName.BEAR, "a hostile bear", null, null, 200, 40, 10);
        blackKnight = new Enemy(CharacterName.BLACK_KNIGHT, "a tall black knight", null, null, 400, 50, 15); 
        aligator = new Enemy(CharacterName.ALIGATOR, "a starving aligator", null, null, 500, 55, 40);
        tarentula = new Enemy(CharacterName.TARENTULA, "a hairy and poisonous tarentula", null, null, 600, 60, 45);
        dragon = new Enemy(CharacterName.DRAGON, "a hostile dragon", null, null, 1000, 90, 60);
        gateKeeper = new Enemy(CharacterName.GATE_KEEPER, "the gatekeeper", null, null, 300, 50, 35);
        fantom = new Enemy(CharacterName.FANTOM, "a fantom", null, null, 200, 1000000, 15);
        gobelin = new Enemy(CharacterName.GOBELIN, "a thieft gobelin", null, null, 200, 50, 25);
        
        gandalf = new Friend(CharacterName.GANDALF, "the reknown Gandalf the Purple", null, null, 10000);
        prisoner = new Friend(CharacterName.PRISONER, "a starving prisoner", null, null, 0);
        
        bartender = new Merchant(CharacterName.BARTENDER, "a cool bartender", null, null, 20000);
        
        forest.addItem(slingshot);
        
        hutYard.addItem(lantern);
        
        hut.addItem(rope);
        hut.addItem(knife);
        hut.addItem(flute);
        
        cavern.addItem(rock);
        
        mountain.addItem(fruit);
        //mountain.addItem(bearskin);
        
        magicRoom.addItem(bag);
        magicRoom.addItem(gourd);
        magicRoom.addItem(redDoor);
        magicRoom.addItem(greenDoor);
        magicRoom.addItem(brownDoor);
        magicRoom.addItem(map);
        
        waterMill.addItem(wood);
        
        castleShore.addItem(axe);
        
        banquetHall.addItem(bread);
        banquetHall.addItem(wine);
        banquetHall.addItem(cheese);
        
        arsenal.addItem(sword);
        arsenal.addItem(arbalet);
        
        office.addItem(key);
        
        //throneRoom.addItem(crown);
        
        // add items to characters
        
        bear.addItem(bearskin);
        
        bartender.addItem(beer);
        bartender.addItem(potion);
        
        dragon.addItem(crown);
        
        // add characters to rooms
        
        mountain.addCharacter(bear);
        
        waterMill.addCharacter(blackKnight);
        
        office.addCharacter(gateKeeper);
        
        banquetHall.addCharacter(fantom);
        
        pub.addCharacter(bartender);
        
        arsenal.addCharacter(tarentula);
        
        sewers.addCharacter(aligator);
        
        prison.addCharacter(prisoner);
        
        throneRoom.addCharacter(dragon);
        
        player.setCurrentRoom(forest);       
    }

    // Initialize the first room.
    private void setFirstOutput() {
        engine.appendToOutputString("Welcome to the World of Zuul!\n");
        engine.appendToOutputString("World of Zuul is a great adventure game.\n");
        engine.appendToOutputString("Type 'help' if you need help.\n");
    }
}
