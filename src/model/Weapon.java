/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.awt.Point;

/**
 *
 * @author djayb6
 */
public class Weapon extends Item {
   
    private int powerAttack;

    public Weapon(String name, String description, int weight, int price, boolean canBeTaken, String imagePath, Point position, int powerAttack) {
        
        super(name, description, weight, price, canBeTaken, imagePath, position);
        this.powerAttack = powerAttack;
    }

    public int getPowerAttack() {
        return powerAttack;
    }
}
