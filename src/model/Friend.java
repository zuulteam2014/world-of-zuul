/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.awt.Point;

/**
 *
 * @author djayb6
 */
public class Friend extends Character {

    public Friend(String name, String description, String imagePath, Point position, int gold) {
        super(name, description, imagePath, position, gold);
    }
    
    
    @Override
    public boolean isEnemy()       
    {
        return false;
    }

    @Override
    public boolean isMerchant() {
        return false;
    }
    
    
}
