/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author djayb6
 */
public class RoomName {
    
    
   public static final String FOREST = "Forest";
   public static final String HUT_YARD = "Hut Yard";
   public static final String HUT = "Hut";
   public static final String CAVERN = "Cavern";
   public static final String MOUNTAIN = "Mountain";
   public static final String MAGIC_ROOM = "Magic Room";
   public static final String WATER_MILL = "Watermill";
   public static final String PONTOON = "Pontoon";
   public static final String RIVER = "River";
   public static final String CASTLE_SHORE = "Castle Shore";
   public static final String LOBBY = "Lobby";
   public static final String BANQUET_HALL = "Banquet Hall";
   public static final String ARSENAL = "Armory";
   public static final String OFFICE = "Office";
   public static final String PUB = "Pub";
   public static final String STAIRS_ROOM = "Stairs Room";
   public static final String SEWERS = "Sewers";
   public static final String PRISON = "Prison";
   public static final String THRONE_ROOM = "Throne Room";
    
    
    
}
