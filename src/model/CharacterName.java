/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author djayb6
 */
public class CharacterName {
    
    public static final String BEAR = "bear";
    public static final String BARTENDER = "bartender";
    public static final String GANDALF = "gandalf";
    public static final String PRISONER = "prisoner";
    public static final String DRAGON = "dragon";
    public static final String ALIGATOR = "aligator";
    public static final String TARENTULA = "tarentula";
    public static final String BLACK_KNIGHT = "blackknight";
    public static final String FANTOM = "fantom";
    public static final String GATE_KEEPER = "gatekeeper";
    public static final String GOBELIN = "gobelin";
    
    public static final String MERCHANT = "merchant";
   
}
