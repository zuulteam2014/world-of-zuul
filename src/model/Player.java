package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class represents players in the game. Each player has a current location
 * and also a list contains which rooms he where in before.
 */
public class Player implements Serializable {
    
    private String name;
    private Room currentRoom;
    private ArrayList<Room> previousRooms;
    private ArrayList<Item> items;
    private int life;
    private int gold;
    private int attackPower;
    private final int maxWeight = 15000; // 15kg
    private long startTime;
    private long elapsedTime;
    private boolean hasMetFantom;
    private boolean gotHintFromPrisoner;
    
    //This is Constructor.
    public Player(String name) {
       this.name = name;
        currentRoom = null;
        previousRooms = new ArrayList<Room>();
        items = new ArrayList<Item>();
        life = 100;
        gold = 100;
        attackPower = 10;
        elapsedTime = 0;
        startTime = System.nanoTime();
        hasMetFantom = false;
        gotHintFromPrisoner = false;
    }

    public int getAttackPower()
    {
        return attackPower;
    }
    
    public int getCurrentWeight()
    {
        int totalWeight = 0;
        
        for (Item item : items)
        {
            totalWeight += item.getWeight();
        }
        
        return totalWeight;
    }
    // Return the current room for this player.
    public Room getCurrentRoom() {
        return currentRoom;
    }

    //Set the current room for this player.     
    public void setCurrentRoom(Room room) {
        currentRoom = room;
    }

    // Returns the list of previous room that this player where in before.
    public ArrayList getPreviousRooms() {
        return previousRooms;
    }
    
    public void setPreviousRooms(ArrayList previousRooms)
    {
        this.previousRooms = previousRooms;
    }
    
    // Adds the list of previously visited rooms.
    public void addPreviousRoom(Room room) {
        previousRooms.add(room);
    }
    
    public Room getLastRoom()
    {
        return previousRooms.get(previousRooms.size()-1);
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }
    
    public boolean isDead()
    {
        return (life <= 0);
    }
    
    public void decreaseLife(int amount)
    {
        //this.life -= amount;
        if (amount > life)
            life = 0;
        else life -= amount;
                    
    }
    
    public void increaseLife(int amount)
    {
        //this.life -= amount;
        if (life+amount >= 100)
            life = 100;
        else life += amount;
                    
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
    
    public void increaseGold(int amount)
    {
        gold += amount;
    }
    
    public void decreaseGold(int amount)
    {
        gold -= amount;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {
        this.items.add(item);
    }
    
    public void setItems(ArrayList items)
    {
        this.items = items;
    }
    
    public boolean canAddItem(Item item)
    {
        return (getCurrentWeight() + item.getWeight() <= maxWeight);
    }
    
    public void removeItem(Item theItem)
    {
        for (Iterator i = items.iterator(); i.hasNext(); ) {
            Item item = (Item)i.next();

            if (item.equals(theItem)) {
                i.remove();
                return;
            }
        }
      
    }
    
    public Item getItemWithName(String name)
    {
        for (Item item : items)   
        {
            if (item.getName().equals(name))
                return item;
        }
        
        return null;
    }
    
    public Weapon getWeaponWithName(String name)
    {
        for (Item item : items)
        {
            if (item.getName().equals(name) && item.getClass().getSimpleName().equals("Weapon"))
                return (Weapon)item;
        }
        
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void setElapsedTime(long elapsed)
    {
        elapsedTime = elapsed;
    }
    
    public void setStartTime(long startTime)    
    {
        this.startTime = startTime;
    }
    
    public long getElapsedTime()
    {
        return elapsedTime;
    }
    
    public long getStartTime()
    {
        return startTime;
    }

    public boolean hasMetFantom() {
        return hasMetFantom;
    }

    public void setHasMetFantom(boolean hasMetFantom) {
        this.hasMetFantom = hasMetFantom;
    }

    public boolean gotHintFromPrisoner() {
        return gotHintFromPrisoner;
    }

    public void setGotHintFromPrisoner(boolean gotHintFromPrisoner) {
        this.gotHintFromPrisoner = gotHintFromPrisoner;
    }
    
    
}
