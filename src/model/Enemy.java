/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.awt.Point;

/**
 *
 * @author djayb6
 */
public class Enemy extends Character {
    
    private int life;
    private int attackPower;

    public Enemy(String name, String description, String imagePath, Point position, int gold, int life, int attackPower) {
        super(name, description, imagePath, position, gold);
        this.life = life;
        this.attackPower = attackPower;
    }

    public int getAttackPower() {
        return attackPower;
    }   
    
    public boolean isDead()
    {
        return (life <= 0);
    }
    
    public void decreaseLife(int amount)
    {
        //this.life -= amount;
        if (amount > life)
            life = 0;
        else life -= amount;
                    
    }

    @Override
    public boolean isEnemy() {
        return true;
    }

    @Override
    public boolean isMerchant() {
        return false;
    }
    
}
