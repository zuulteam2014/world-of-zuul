/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.awt.Point;

/**
 *
 * @author djayb6
 */
public class LifeItem extends Item {

    private int lifeGain;
    
    public LifeItem(String name, String description, int weight, int price, boolean canBeTaken, String imagePath, Point position, int lifeGain) {
        super(name, description, weight, price, canBeTaken, imagePath, position);
        this.lifeGain = lifeGain;
    }
    
    public int getLifeGain()
    {
        return lifeGain;
    }
    
}
