/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author djayb6
 */
public class ItemName {
     
     public static final String WOOD       = "wood";
     public static final String ROPE       = "rope";
     public static final String ROCK       = "rock";
     public static final String KEY        = "key";
     public static final String BREAD      = "bread";
     public static final String WINE       = "wine";
     public static final String FRUIT      = "fruit"; 
     public static final String CHEESE     = "cheese";
     public static final String POTION     = "potion";
     public static final String BEER       = "beer";
     public static final String MAP        = "map";
     public static final String BAG        = "bag";
     public static final String LANTERN    = "lantern";
     public static final String FLUTE      = "flute";
     public static final String BEARSKIN   = "bearskin";
     public static final String GOURD      = "gourd";
     public static final String MONEY      = "money"; 
     public static final String RAFT       = "raft";
     public static final String RED_DOOR   = "red door";
     public static final String GREEN_DOOR = "green door";
     public static final String BROWN_DOOR = "brown door";
     public static final String CROWN = "crown";
     
     public static final String SLINGSHOT   = "slingshot";
     public static final String KNIFE  = "knife";
     public static final String SWORD  = "sword";
     public static final String AXE    = "axe";
     public static final String ARBALET = "arbalet";
}
