package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/*
 * Class Room - a room in an adventure game.
 *
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 *
 * A "Room" represents one location in the scenery of the game. It is connected
 * to other rooms via exits. For each existing exit, the room stores a reference
 * to the neighboring room.
 *
 * @author Michael Kolling and David J. Barnes
 */
public class Room implements Serializable {

    private String name;
    private String description;
    // stores exits of this room.
    private HashMap<String, Room> exits;
    private String imageName;
    private ArrayList<Item> items;
    private ArrayList<Character> characters;
    private boolean locked;

    /*
     * Create a new room described "description" with a given image. Firstly, it
     * has no exits.
     */
    public Room(String name, String description, String imageName) {
        this.description = description;
        this.imageName = imageName;
        this.name = name;
        exits = new HashMap<>();
        items = new ArrayList<>();
        characters = new ArrayList<>();
        locked = false;
       
    }
    
    public boolean isLocked()
    {
        return locked;
    }
    
    public void lock()
    {
        this.locked = true;
    }
    
    public void unlock()
    {
        this.locked = false;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public void addItem(Item item)
    {
        items.add(item);
    }
    
    public ArrayList getItems()
    {
        return items;
    }
    
    public void removeItem(Item theItem)
    {
        for (Iterator i = items.iterator(); i.hasNext(); ) {
            Item item = (Item)i.next();

            if (item.equals(theItem)) {
                i.remove();
                return;
            }
        }
    }
    
    public Item getItemWithName(String name)
    {
        for (Item item : items)   
        {
            if (item.getName().equals(name))
                return item;
        }
        
        return null;
    }
    
    public void addCharacter(Character character)
    {
        characters.add(character);
    }
    
    public ArrayList getCharacters()
    {
        return characters;
    }
    
    public Character getCharacterWithName(String name)
    {
        for (Character character : characters)
        {
            if (character.getName().equals(name))
                return character;
        }
        
        return null;
    }
    
    public void removeCharacter(Character theChar)
    {
        for (Iterator i = characters.iterator(); i.hasNext(); ) {
            Character character = (Character)i.next();

            if (character.equals(theChar)) {
                i.remove();
                return;
            }
        }
    }

    // Defines an exit from this room.
    public void setExit(String direction, Room neighbor) {
        exits.put(direction, neighbor);
    }

    // Returns the description of the room (the one that was defined in the Constructor).
    public String getDescription() {
        return description;
    }

    // Returns room exits.
    public HashMap getExits() {
        return exits;
    }

    /*
     * Return the room that is reached if we go from this room in direction
     * "direction". If there is no room in that direction, return null.
     */
    public Room getExit(String direction) {
        return exits.get(direction);
    }

    // Return a string describing the room's image name
    public String getImageName() {
        return imageName;
    }
    
    public void setImageName(String imageName)
    {
        this.imageName = imageName;
    }
    
    public Room getNeighborWithName(String name)
    {
        for (Entry<String, Room> entry : exits.entrySet()) 
        {     
            if (entry.getValue().getName().equals(name))
                return entry.getValue();
                    
        }
        
        return null;
    }
}
