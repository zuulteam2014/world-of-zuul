/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author djayb6
 */
public abstract class Character implements Serializable {
    
    private String name;
    private String description;
    private String imagePath;
    private Point position;
    private ArrayList<Item> items;
    private int gold;

    public Character(String name, String description, String imagePath, Point position, int gold) {
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.position = position;
        this.items = new ArrayList<>();
        this.gold = gold;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public ArrayList getItems() {
        return items;
    }

    public void addItem(Item item) {
        this.items.add(item);
    }
    
    public Item getItemWithName(String name)
    {
        for (Item item : items)   
        {
            if (item.getName().equals(name))
                return item;
        }
        
        return null;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
    
    public abstract boolean isEnemy();
    public abstract boolean isMerchant();
}

