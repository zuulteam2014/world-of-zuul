/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author djayb6
 */
public class Scores {
    
    private HashMap<String, Long> scores;

    public Scores() {
        scores = new HashMap<>();
    }
    
    
    
    public HashMap<String, Long> getScores()
    {
        String folder = System.getProperty("user.home") + File.separator + "zuul";
        String scoresPath = folder + File.separator + "scores.hm";
        //new File(folder).mkdirs;
        
        if (!new File(folder).exists())
            return null;
        
        FileInputStream fis;
        try {
            fis = new FileInputStream(scoresPath);
            ObjectInputStream ois =  new ObjectInputStream(fis);
           
           scores = (HashMap<String, Long>)ois.readObject();
           
           ois.close();
           fis.close();
           
           return scores;
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Scores.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        
    }
    
    public void saveScores()
    {
        try {
            String folder = System.getProperty("user.home") + File.separator + "zuul";
            String scoresPath = folder + File.separator + "scores.hm";
            new File(folder).mkdirs();

            FileOutputStream fos = new FileOutputStream(scoresPath);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(scores);

            oos.close();
            fos.close();

            System.out.println("Scores saved to " + scoresPath);
        } catch (IOException ex) {
            Logger.getLogger(Scores.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Failed to save game");
        }
    }
    
    public void addScore(String name, long score)
    {
        getScores();
        
        scores.put(name, score);
        
        saveScores();
        
        
        
    }
    
}
