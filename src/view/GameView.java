package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicProgressBarUI;
import model.Character;
import model.GameEngine;
import model.Item;
import model.Room;

/**
 * This class implements a simple graphical user interface with a text entry
 * area, a text output area and an optional image.
 */
public class GameView implements ActionListener, GameListener, ListSelectionListener{

    /*
     * Lists of Strings holding the data to be displayed.
     * The content of these lists will automatically be displayed in the GUI.
     * IMPORTANT: DO NOT MODIFY NAMES OR TYPES OF THESE 5 FIELDS!!!
     *
     */
    private ArrayList<String> myRoomItems, myRoomCharacters, myPlayerItems, myPlayerStats;
    private URL roomImage, playerImage;
    private GameEngine engine;
    /**
     * Construct a UserInterface. As a parameter, a Game Engine
     * (an object processing and executing the game commands) is
     * needed.
     * 
     * @param engine  The GameEngine object implementing the game logic.
     * @param myframe
     */
    public GameView(GameEngine engine, JFrame myframe) {
        this.engine = engine;
        // These ArrayLists contains the list of items available in the room, items that the player carry and the stats of the players respectively.
        myRoomItems = new ArrayList<String>();
        myRoomCharacters = new ArrayList<String>();
        myPlayerItems = new ArrayList<String>();
        myPlayerStats = new ArrayList<String>();

        roomImage = null;
        playerImage = null;

        createGUI(myframe);

        engine.addGameListener(this);
        // 'auto-invoke' method at start up since nothing has yet been displayed

    }

    /**
     * Sets the list of room items. If you want to show items of current room, you should modify this method.
     */
    private void setRoomItems() {
        myRoomItems.clear();
        
        if (engine.getPlayer().getCurrentRoom().getItems().isEmpty())
        {
        
            myRoomItems.add("none");
        } else
        {
            ArrayList<Item> items = engine.getPlayer().getCurrentRoom().getItems(); 
            
            for (Item item : items)
            {
                myRoomItems.add(item.getName());
             }
        }
    }

    
    private void setRoomCharacters() {
        myRoomCharacters.clear();
        
        if (engine.getPlayer().getCurrentRoom().getCharacters().isEmpty())
        {
        
            myRoomItems.add("none");
        } else
        {
            ArrayList<Character> characters = engine.getPlayer().getCurrentRoom().getCharacters(); 
            
            for (Character character : characters)
            {
                myRoomCharacters.add(character.getName());
             }
        }
    }
    
    /**
     * Sets the list of player items. If you want to show the items of the player, you should modify this method.
     */
    private void setPlayerItems() {
        myPlayerItems.clear();
        
        if (engine.getPlayer().getItems().isEmpty())
        {
            myPlayerItems.add("none");
        } else
        {
            ArrayList<Item> items = engine.getPlayer().getItems();
            for (Item item : items)
            {
                myPlayerItems.add(item.getName());
             }
        }
        
        
        
        
    }

    /**
     * Sets the list of player states. If you want to change the status of the player, you should modify this method.
     */
    private void setPlayerStats() {
        //myPlayerStats.clear();
        //myPlayerStats.add("none");
        /*
        myPlayerItems.add("Life: " + engine.getPlayer().getLife());
        myPlayerItems.add("Gold: " + engine.getPlayer().getGold());
        myPlayerItems.add("Items weight: " + engine.getPlayer().getCurrentWeight());
        */
        
    }

    /**
     * Sets the name of the room image to be displayed.
     */
    private void setRoomImage(String roomImageName) {
        roomImage = getClass().getResource(roomImageName);
    }

    /**
     * Sets the name of the player image to be displayed. If you want to change the player picture, you should modify this method.
     */
    private void setPlayerImage() {
        playerImage = getClass().getResource("/images/knight.gif");
    }
    //method for creating and resizing the icons of the direction buttons
    private ImageIcon getDirectionButtonImage(String direction){
        URL url = getClass().getResource("/images/"+direction+".png");
        BufferedImage image = null;
        BufferedImage resizedImage = null;
        ImageIcon icon = null;
            try {
                image = ImageIO.read(url);
                resizedImage = resize(image, 120, 120);
                icon = new ImageIcon(resizedImage);
            } catch (IOException ex) {
                Logger.getLogger(GameView.class.getName()).log(Level.SEVERE, null, ex);
            }
        return(icon);
    }
// ==================LISTENERS' METHODS==============================
    /**
     * Method triggered when eventListener is notified that an event has happened (here, event = 'enter' key typed in text field).
     * @param e action event
     */
    
    //also listen to gamemenu, actions implemented here
    public void actionPerformed(ActionEvent e) {
        // no need to check the type of action at the moment.
        // there is only one possible action: text entry
        if(e.getSource() == menuButton) showmenu();
        else if(e.getSource() == gamemenu.continueButton) hidemenu();
        else if(e.getSource() == mapButton) showmap();
        else if(e.getSource() == mapView) hidemap();
        else if(e.getSource() == east) engine.interpretCommand("go east");
        else if(e.getSource() == west) engine.interpretCommand("go west");
        else if(e.getSource() == north) engine.interpretCommand("go north");
        else if(e.getSource() == south) engine.interpretCommand("go south");
        else if(e.getSource() == gamemenu.saveButton) engine.interpretCommand("save");
        else if(e.getSource() == gamemenu.exitButton) System.exit(0);
        else if(e.getSource() == characterlabel) showcharactermenu(currentcharacter);
        else if(e.getSource() == hidemenu) hidecharactermenu();
        else processCommand();
    }
    //commands processing
    private void processCommand() {
        String input = inputBox.getText();
        inputBox.setText("");
        engine.interpretCommand(input);
    }

    /**
     * Method triggered when gameListener is notified that the game has been modified.
     * @param roomImageName the name of the image of the room to be displayed
     */
    public void gameStateModified(String roomImageName) {

        // Tests if game is over, and disables the text field if it's the case.
        if (engine.isFinished()) {
            enable(false);
        }

        // Clears the text displayed and then gets the new data in the Engine.
        clearLog();
        println(engine.getOutputString());

        // Sets fields to possibly new values.
        setRoomItems();
        setRoomCharacters();
        setPlayerItems();
        setPlayerStats();
        setRoomImage(roomImageName);
        setPlayerImage();
        // Updates images and lists displayed in the GUI.
        // IMPORTANT: Fields of the GUI must have been set to new values before.
        updateRoomImage();
        updateLists();
        updatestats();
        updateDirectionButtons();
    }
//==============================================================================
//  << NO NEED TO READ THE FOLLOWING CODE >> << MODIFY AT YOUR OWN RISK >>
//==============================================================================
// ==================FIELDS==============================
    //variables declaration
    //background panel
    private JLayeredPane mainPanel;
    //sub panels and their components.
    //ROOM
    private JPanel roomPanel;
    private JLabel mapLabel;
    private JPanel roomlistPanel;
    private JScrollPane listScroller3;
    private JList<String>  roomItems;
    private JList charactersInDaRoom;
    private JPanel charactersPanel;
    //PLAYER
    private JPanel playerPanel;
    private JScrollPane listScroller2;
    private JList playerStats;
    private JPanel listPanel;
    private JScrollPane listScroller4;
    private JList playerItems;
    //WIZARD
    private JPanel dialogPanel;
    private JTextField inputBox;
    private JScrollPane listScroller;
    private JTextArea log;
    //Layout variable
    private JFrame myFrame;
    private StatsPanel statsPanel;
    private JButton menuButton;
    private JButton mapButton;
    private GameMenuPanel gamemenu;
    model.Character currentcharacter;
    private JButton characterlabel;
    private JPopupMenu charactermenu;
    private JMenuItem hidemenu;
    private JPanel savepanel;
    private JTextField savetext;
    private JButton north, east, south, west;
    private JProgressBar weightbar;
    private DefaultBoundedRangeModel weightmodel;
    private Font font;
    private JLabel gandalflabel;
    private JButton mapView;
// ==================METHODS==============================
    /**
     * initializes all components of the GUI
     */
    private void createGUI(JFrame frame) {
        //background panel
        mainPanel = new JLayeredPane();
        myFrame = frame;
        //6 containers (panels)
        roomPanel = new JPanel();
        playerPanel = new JPanel();
        dialogPanel = new JPanel();
        listPanel = new JPanel();
        charactersPanel = new JPanel();
        roomlistPanel = new JPanel();
        statsPanel = new StatsPanel();
        menuButton = new JButton();
        mapButton = new JButton();
        mapView = new JButton();
        gamemenu = new GameMenuPanel(this);
        characterlabel = new JButton();
        charactermenu = new JPopupMenu("You can interact with this character: ");
        weightbar = new JProgressBar();
        
        //creation of gandalf label
        gandalflabel = new JLabel();
        gandalflabel.setIcon(new ImageIcon(getClass().getResource("/images/gandalf.png")));
        gandalflabel.setOpaque(false);
        gandalflabel.setSize(320,480);
        
        //creation of mapView
        mapView.setIcon(new ImageIcon(getClass().getResource("/images/map.png")));
        mapView.setOpaque(false);
        mapView.setSize(800,302);
        
        //Creation of the direction buttons
        east = new JButton();
        west = new JButton();
        north = new JButton();
        south = new JButton();
        east.setSize(120, 80);
        east.setIcon(getDirectionButtonImage("east"));
        east.setContentAreaFilled(false);
        east.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        east.setFocusPainted(false);
        east.addActionListener(this);
        west.setSize(120, 80);
        west.setIcon(getDirectionButtonImage("west"));
        west.setContentAreaFilled(false);
        west.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        west.setFocusPainted(false);
        west.addActionListener(this);
        north.setSize(120, 80);
        north.setIcon(getDirectionButtonImage("north"));
        north.setContentAreaFilled(false);
        north.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        north.setFocusPainted(false);
        north.addActionListener(this);
        south.setSize(120, 80);
        south.setIcon(getDirectionButtonImage("south"));
        south.setContentAreaFilled(false);
        south.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        south.setFocusPainted(false);
        south.addActionListener(this);        
        font = new Font("verdana", font.BOLD, 12);
        //add weightbar
        weightbar.setSize(170, 28);
        weightbar.setFont(font);
        weightbar.setString("Weight");
        weightbar.setStringPainted(true);
        weightbar.setOpaque(false);
        weightmodel = new DefaultBoundedRangeModel(0,1,0,15000);
        weightbar.setModel(weightmodel);
        weightbar.setForeground(new Color(173, 126, 54, 220));
        weightmodel.setValue(0);
        weightbar.setString("Weight: "+weightbar.getModel().getValue()+" / 15000g ");
        weightbar.setUI(new BasicProgressBarUI() {
            protected Color getSelectionBackground() { return Color.black; }
            protected Color getSelectionForeground() { return Color.black; }
        });
        
        //Create charactermenu interaction possible options
        hidemenu = new JMenuItem("hide menu");
        hidemenu.addActionListener(this);

        myFrame.getContentPane().add(mainPanel);    
        
        //preparing characterlabel for methods showcharacter and hidecharacter
        characterlabel.setOpaque(false);
        mainPanel.add(characterlabel, JLayeredPane.PALETTE_LAYER);
        characterlabel.addActionListener(this);
        mainPanel.add(charactermenu, JLayeredPane.POPUP_LAYER);
        charactermenu.setBorderPainted(true);
        //charactermenu.addMenuKeyListener(this);
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------
        //subcontainers 
        //adding the various components to the subcontainers

        //this method 
        

        roomItems = new JList();
        //roomItems.setBackground(new Color(173, 126, 54, 50));
        roomItems.addListSelectionListener(this);
        listScroller3 = new JScrollPane();
        listScroller3.getViewport().add(roomItems);
        listScroller3.setPreferredSize(new Dimension(120, 80));
        listScroller3.setMinimumSize(new Dimension(120, 80));
      
        //roomItems.setOpaque(false);
        roomlistPanel.add(listScroller3);
        roomlistPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Items in the room"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));


        
         //text boxes
        log = new JTextArea();
        log.setEditable(false);

        listScroller = new JScrollPane(log);
        listScroller.setPreferredSize(new Dimension(180, 100));
        listScroller.setMinimumSize(new Dimension(180, 100));

        inputBox = new JTextField(37);

        dialogPanel.setLayout(new BorderLayout());
        dialogPanel.add(listScroller, BorderLayout.NORTH);
        dialogPanel.add(inputBox, BorderLayout.CENTER);


        playerItems = new JList();
        listScroller2 = new JScrollPane();
        listScroller2.getViewport().add(playerItems);
        listScroller2.setPreferredSize(new Dimension(120, 80));
        listScroller2.setMinimumSize(new Dimension(120, 80));

        listPanel.add(listScroller2);
        listPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Player Items"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        charactersInDaRoom = new JList();
        listScroller4 = new JScrollPane();
        listScroller4.getViewport().add(charactersInDaRoom);
        listScroller4.setPreferredSize(new Dimension(120, 80));
        listScroller4.setMinimumSize(new Dimension(120, 80));

        charactersPanel.add(listScroller4);
        charactersPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Characters"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
  


        playerPanel.setLayout(new FlowLayout());
        listPanel.setOpaque(false);
        playerPanel.add(listPanel);
        playerPanel.add(dialogPanel); 
        playerPanel.add(roomlistPanel);
        playerPanel.add(charactersPanel);
        
       

        /*roomPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(" ROOM "),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        */

        //playerPanel.setBorder(BorderFactory.createTitledBorder(" PLAYER "));
        mapLabel = new JLabel();

        mainPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
       
        // Adding components to mainPanel JLayered Pane
        
        mainPanel.add(mapLabel, JLayeredPane.DEFAULT_LAYER);
        mapLabel.setSize(new Dimension(960, 540));
        mapLabel.setLocation(0, 0);
        mapLabel.setVisible(true);
        mainPanel.add(east, JLayeredPane.PALETTE_LAYER);
        east.setLocation(840, 210);
        east.setVisible(false);
        mainPanel.add(west, JLayeredPane.PALETTE_LAYER);
        west.setLocation(10, 210);
        west.setVisible(false);
        mainPanel.add(south, JLayeredPane.PALETTE_LAYER);
        south.setLocation(420, 450);
        south.setVisible(false);
        mainPanel.add(north, JLayeredPane.PALETTE_LAYER);
        north.setLocation(420, 20);
        north.setVisible(false);
        mainPanel.add(playerPanel, JLayeredPane.PALETTE_LAYER);
        
        playerPanel.setSize(new Dimension(960, 150));
        playerPanel.setLocation(0, 545);
        playerPanel.setOpaque(false);
        playerPanel.setVisible(true);
        /*mainPanel.add(roomlistPanel, JLayeredPane.PALETTE_LAYER);
        roomlistPanel.setSize(150, 150);
        roomlistPanel.setLocation(800, 40);
        roomlistPanel.setOpaque(false);
        roomlistPanel.setVisible(true);
        */
        mainPanel.add(statsPanel, JLayeredPane.PALETTE_LAYER);
        statsPanel.setLocation(10, 10);
        statsPanel.setOpaque(false);
        statsPanel.setVisible(true);
        mainPanel.add(weightbar, JLayeredPane.PALETTE_LAYER);
        weightbar.setLocation(270, 11);
        
        menuButton.setText("Menu");
        menuButton.addActionListener(this);
        menuButton.setSize(100, 30);
        mainPanel.add(menuButton, JLayeredPane.PALETTE_LAYER);
        menuButton.setLocation(800, 5);
        
        mapButton.setText("Map");
        mapButton.addActionListener(this);
        mapButton.setSize(100, 30);
        mainPanel.add(mapButton, JLayeredPane.PALETTE_LAYER);
        mapButton.setLocation(630, 5);
        
        //adding gandalf but not showing
        mainPanel.add(gandalflabel, JLayeredPane.PALETTE_LAYER);
        gandalflabel.setLocation(200, 100);
        gandalflabel.setVisible(false);
        
        //adding map but not showing
        mapView.addActionListener(this);
        mainPanel.add(mapView, JLayeredPane.PALETTE_LAYER);
        mapView.setLocation(80, 100);
        mapView.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        mapView.setFocusPainted(false);
        mapView.setVisible(false);
        

        mainPanel.add(gamemenu, JLayeredPane.POPUP_LAYER);
        gamemenu.setLocation(280,175);
        gamemenu.setVisible(false);

        myFrame.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        //currentcharacter = new model.Friend("dragon", null, null, null, 9); // for test purposes only
        //showcharacter(currentcharacter, "/images/dragon.jpg", 80, 180); // for test purposes only
        
        inputBox.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                int keyCode = keyEvent.getKeyCode();
                switch (keyCode) {
                    case KeyEvent.VK_UP:
                        // handle up
                        engine.interpretCommand("go north");
                        break;
                    case KeyEvent.VK_DOWN:
                        // handle down 
                        engine.interpretCommand("go south");
                        break;
                    case KeyEvent.VK_LEFT:
                        // handle left
                        engine.interpretCommand("go west");
                        break;
                    case KeyEvent.VK_RIGHT:
                        // handle right
                        engine.interpretCommand("go east");
                        break;
                }
                //inputBox.requestFocus();

            }
            
            @Override
            public void keyTyped(KeyEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            
            @Override
            public void keyReleased(KeyEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        inputBox.addActionListener(this);
        
        myFrame.pack();
        myFrame.setVisible(true);
        inputBox.requestFocus();

    }
    
    
    //show & hide character methods, arguments are string with path of the icon, and xy location
    public void showcharacter(Character c, String path, int x, int y){
        currentcharacter = c;
        Icon i;
        i = new javax.swing.ImageIcon(getClass().getResource(path));
        characterlabel.setSize(i.getIconWidth(), i.getIconHeight());
        characterlabel.setIcon(i);
        characterlabel.setLocation(x, y);
        characterlabel.setVisible(true);
    }
    
    public void hidecharacter(){
        characterlabel.setVisible(false);
    }
    
    public void showcharactermenu(Character c){
        //must add possible actions here
        //if(c.ennemy) charactermenu.add(fight)  NEED TO ADD JMENUITEM fightmenu FIRST!!!!
        charactermenu.add(hidemenu);
        charactermenu.setPopupSize(100, characterlabel.getHeight());
        charactermenu.setLocation(characterlabel.getLocation().x + characterlabel.getWidth() + 5, characterlabel.getLocation().y + 26);
        charactermenu.setVisible(true);
    }
    
    public void updatestats(){
        
        statsPanel.setLife(engine.getPlayer().getLife());
        statsPanel.setGold(engine.getPlayer().getGold());
        weightmodel.setValue(engine.getPlayer().getCurrentWeight());
        weightbar.setString(" Weight: "+weightmodel.getValue()+" / 15000g ");
    }
    
    public void hidecharactermenu(){
        charactermenu.removeAll();
        charactermenu.setVisible(false);
        //charactermenu.updateUI();
        //mainPanel.repaint();
        
    }
    
    // show & hide menu methods
    public void showmenu(){
        gamemenu.setVisible(true);
    }
    
    public void hidemenu(){
        gamemenu.setVisible(false);
    }
    
    public void load(String s){
        engine.interpretCommand("load "+s);
    }
    /**
     * Udates the Jlists in the GUI with the content of the three arrayList fields.
     */
    private void updateLists() {
        String[] myList1 = new String[myRoomItems.size()];
        for (int index = 0; index < myList1.length; index++) {
            myList1[index] = myRoomItems.get(index);
        }
        roomItems.setListData(myList1);

        String[] myList2 = new String[myPlayerItems.size()];
        for (int index = 0; index < myList2.length; index++) {
            myList2[index] = myPlayerItems.get(index);
        }
        playerItems.setListData(myList2);
   
        String[] myList3 = new String[myRoomCharacters.size()];
        for (int index = 0; index < myList3.length; index++) {
            if(myRoomCharacters.get(index) == model.CharacterName.GANDALF) gandalflabel.setVisible(true);
            myList3[index] = myRoomCharacters.get(index);
        }
        charactersInDaRoom.setListData(myList3);
        
    }

    /**
     * Updates the room image file in the GUI.
     */
    private void updateRoomImage() {
        if (roomImage == null) {
            System.out.println("image not found");
        } else {
            
            BufferedImage image;
            try {
                image = ImageIO.read(roomImage);
                BufferedImage resizedImage = resize(image, mapLabel.getWidth(), mapLabel.getHeight());
                ImageIcon icon = new ImageIcon(resizedImage);
                mapLabel.setIcon(icon);
            } catch (IOException ex) {
                Logger.getLogger(GameView.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            }
    }
    
    public static BufferedImage resize(BufferedImage image, int width, int height)
    {
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
        Graphics2D g2d = (Graphics2D) bi.createGraphics();
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        g2d.drawImage(image, 0, 0, width, height, null);
        g2d.dispose();
        return bi;
    }
    

    /**
     * Updates the player image file in the GUI.
     */

    /**
     * Enable or disable input in the input field.
     * @param on
     */
    public void enable(boolean on) {
        inputBox.setEditable(on);
        if (!on) {
            inputBox.getCaret().setBlinkRate(0);
            
            for (KeyListener k : inputBox.getKeyListeners())
            {
              inputBox.removeKeyListener(k);
            }
        }
    }

    /**
     * Print out some text into the text area.
     * @param text
     */
    public void print(String text) {
        log.append(text);
        log.setCaretPosition(log.getDocument().getLength());
    }

    /**
     * Print out some text into the text area, followed by a line break.
     * @param text 
     */
    public void println(String text) {
        log.append(text + "\n");
        log.setCaretPosition(log.getDocument().getLength());
    }

    /**
     * Clears out the text area.
     */
    private void clearLog() {
        log.setText("");
        log.setCaretPosition(0);
    }
//==============================================================================

    private void updateDirectionButtons() {
        east.setVisible(false);
        west.setVisible(false);
        north.setVisible(false);
        south.setVisible(false);
        HashMap exits =  engine.getPlayer().getCurrentRoom().getExits();
        if(exits.containsKey("east")) east.setVisible(true);
        if(exits.containsKey("west")) west.setVisible(true);
        if(exits.containsKey("north")) north.setVisible(true);
        if(exits.containsKey("south")) south.setVisible(true);
        
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getSource() == roomItems && roomItems.getSelectedIndex() >=0) {
            
            if (!myRoomItems.get(roomItems.getSelectedIndex()).equals("none"))
            {
            
                engine.interpretCommand("take "+myRoomItems.get(roomItems.getSelectedIndex()));
            }
        }
    }

    private void showmap() {
        mapView.setVisible(true);
    }
    
    private void hidemap() {
        mapView.setVisible(false);
    }
}
