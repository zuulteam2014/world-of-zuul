/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import zuul.ZuulStart;


/**
 *
 * @author Diego
 */
public class MainMenuPanel extends JLayeredPane implements ActionListener, ListSelectionListener{
    
    private ZuulStart zuul;
    private javax.swing.JButton startButton;
    private javax.swing.JButton loadButton;
    private javax.swing.JButton exitButton;
    private javax.swing.JLabel bgLabel;
    private JLabel logoLabel;
    private JPopupMenu loadmenu;
    private JPanel namepanel;
    private JLabel namelabel;
    private JTextField namefield;
    private JButton namebutton;
    private JList <String> savedgames;
    
    public MainMenuPanel(ZuulStart z){
        zuul = z;
        initComponents();
    }
    
    private void initComponents() {

        exitButton = new javax.swing.JButton();
        loadButton = new javax.swing.JButton();
        startButton = new javax.swing.JButton();
        bgLabel = new javax.swing.JLabel();
        logoLabel = new JLabel();
        loadmenu = new JPopupMenu();
        namepanel = new JPanel();
        namelabel = new JLabel("Enter your name: ");
        namefield = new JTextField();
        namebutton = new JButton("Play!");
        
        
        //creation of namepanel, not visible yet
        namepanel.setSize(300, 30);
        namepanel.setBackground(new Color(173, 126, 54, 200));
        namepanel.setLayout(new BorderLayout());
        namepanel.add(namelabel, BorderLayout.WEST);
        namepanel.add(namefield, BorderLayout.CENTER);
        namebutton.setSize(50, 30);
        namebutton.addActionListener(this);
        namepanel.add(namebutton, BorderLayout.EAST);
        add(namepanel, JLayeredPane.POPUP_LAYER);
        namepanel.setVisible(false);
        namepanel.setLocation(327, 370);
        
        //adding loadmenu, not visible yet
        //loadmenu.add(savedgames);
        
        
        setSize(965, 725);
        bgLabel.setSize(965, 725);
        bgLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mmbg1.jpg")));
        add(bgLabel, JLayeredPane.DEFAULT_LAYER);
        
        logoLabel.setSize(895, 60);
        logoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/wzuul.png")));
        add(logoLabel, JLayeredPane.PALETTE_LAYER);
        logoLabel.setOpaque(false);
        logoLabel.setLocation(35, 10);
        
        //exitButton.setText("Exit");
        exitButton.setSize(350, 60);
        exitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/exit.png")));
        exitButton.setContentAreaFilled(false);
        exitButton.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        exitButton.setFocusPainted(false);
        exitButton.addActionListener(this);

        //loadButton.setText("Load Game");
        loadButton.setSize(350, 60);
        loadButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/loadgame.png")));
        loadButton.setContentAreaFilled(false);
        loadButton.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        loadButton.setFocusPainted(false);
        loadButton.addActionListener(this);

        //startButton.setText("Start Game");
        startButton.setSize(350, 60);
        startButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/startgame.png")));
        startButton.setContentAreaFilled(false);
        startButton.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        startButton.setFocusPainted(false);
        startButton.addActionListener(this);
        
        add(startButton, JLayeredPane.PALETTE_LAYER);
        startButton.setLocation(307, 310);
        add(loadButton, JLayeredPane.PALETTE_LAYER);
        loadButton.setLocation(307, 410);
        add(exitButton, JLayeredPane.PALETTE_LAYER);
        exitButton.setLocation(307, 510);
    }// </editor-fold>                                                              

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == startButton) {
            showNamePanel();
        }
        else if(e.getSource() == namebutton) 
        {
            String name = namefield.getText();
            
            if (name.length() > 0) 
                zuul.startGameGUI(name);
        
        }
        else if(e.getSource() == exitButton) {
            zuul.exit();
        }
        else if(e.getSource() == loadButton) {
            showLoadMenu();
        }
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        loadGame();
    }
    
    private void showNamePanel(){
        if (namepanel.isVisible())
            namepanel.setVisible(false);
        else
            namepanel.setVisible(true);
            
        
        
    }
    
    private void loadGame(){
        
        if (savedgames.getSelectedValue() != null)
            zuul.loadgame(savedgames.getSelectedValue());
    }
   
    private void showLoadMenu(){
        File dir = new File(System.getProperty("user.home")+File.separator+"zuul");
        File[] directoryListing = dir.listFiles();
        int i = 0;
        String[] games = new String[20];
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if(child.getName().endsWith(".save")){
                    games[i] = (child.getName().split("\\.")[0]);
                    i += 1;
                }
            }
        } else {
            games[0] = ("no games found");
        }
        savedgames = new JList(games);
        savedgames.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        savedgames.setLayoutOrientation(JList.VERTICAL);
        savedgames.setVisibleRowCount(5);
        savedgames.setFont(new Font("verdana", Font.BOLD, 14));
        savedgames.addListSelectionListener(this);
        savedgames.setName("Choose saved game:");
        add(savedgames, JLayeredPane.POPUP_LAYER);
        savedgames.setSize(150, 200);
        savedgames.setLocation(700, 410);
        savedgames.setVisible(true);
    }

   
}
