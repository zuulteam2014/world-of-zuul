/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Diego
 */
public class GameMenuPanel extends JLayeredPane implements ListSelectionListener{
    
    JButton continueButton;
    JButton loadButton;
    JButton exitButton;
    JButton saveButton;
    private javax.swing.JLabel bgLabel;
    private javax.swing.JLabel titleLabel;
    private GameView gameview;
    private JList <String> savedgames;

    public GameMenuPanel(GameView gview){
        gameview = gview;
        initComponents();
    }
    
    private void initComponents() {

        exitButton = new javax.swing.JButton();
        loadButton = new javax.swing.JButton();
        titleLabel = new javax.swing.JLabel();
        continueButton = new javax.swing.JButton();
        saveButton = new JButton();
        bgLabel = new javax.swing.JLabel();

        setSize(400, 300);
        setOpaque(true);
        bgLabel.setSize(400, 300);
        bgLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mbg.jpg")));
        add(bgLabel, JLayeredPane.DEFAULT_LAYER);
        
        //exitButton.setText("Exit");
        exitButton.setSize(240, 70);
        exitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/gexit.png")));
        exitButton.setContentAreaFilled(false);
        exitButton.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        exitButton.setFocusPainted(false);
        exitButton.addActionListener(gameview);

        //loadButton.setText("Load Game");
        loadButton.setSize(240, 70);
        loadButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/gload.png")));
        loadButton.setContentAreaFilled(false);
        loadButton.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        loadButton.setFocusPainted(false);
        loadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showLoadMenu();
            }
        });

        //continueButton.setText("Continue Game");
        continueButton.setSize(240, 70);
        continueButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/gcontinue.png")));
        continueButton.setContentAreaFilled(false);
        continueButton.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        continueButton.setFocusPainted(false);
        continueButton.addActionListener(gameview);
        
        saveButton.setSize(240, 70);
        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/gsave.png")));
        saveButton.setContentAreaFilled(false);
        saveButton.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        saveButton.setFocusPainted(false);
        saveButton.addActionListener(gameview);
        
        add(continueButton, JLayeredPane.PALETTE_LAYER);
        continueButton.setLocation(80, 7);
        add(saveButton, JLayeredPane.PALETTE_LAYER);
        saveButton.setLocation(80, 79);
        add(loadButton, JLayeredPane.PALETTE_LAYER);
        loadButton.setLocation(80, 151);
        add(exitButton, JLayeredPane.PALETTE_LAYER);
        exitButton.setLocation(80, 223);
    }// </editor-fold>                        
                                        
    private void showLoadMenu(){
        File dir = new File(System.getProperty("user.home")+File.separator+"zuul");
        File[] directoryListing = dir.listFiles();
        int i = 0;
        String[] games = new String[20];
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if(child.getName().endsWith(".save")){
                    games[i] = (child.getName().split("\\.")[0]);
                    i += 1;
                }
            }
        } else {
            games[0] = ("no games found");
        }
        savedgames = new JList(games);
        savedgames.addListSelectionListener(this);
        savedgames.setName("Choose saved game:");
        add(savedgames, JLayeredPane.POPUP_LAYER);
        savedgames.setSize(100, 75);
        savedgames.setLocation(300, 150);
        savedgames.setVisible(true);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        savedgames.setVisible(false);
        gameview.load(savedgames.getSelectedValue());
        savedgames.removeAll();
    }
}
