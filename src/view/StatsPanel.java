/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicProgressBarUI;

/**
 *
 * @author Diego
 */
public class StatsPanel extends JPanel{
    
    private JLabel jLabel1;
    private JProgressBar jProgressBar1;
    private DefaultBoundedRangeModel model;
    private JLabel goldlabel;
    private JTextField goldtext;
    private JPanel goldpanel;
    private Font font;
    
    public StatsPanel() {
        initComponents();
        initBar();
    }
    
    private void initComponents() {

        jProgressBar1 = new JProgressBar();
        jLabel1 = new JLabel();
        goldlabel = new JLabel();
        goldtext = new JTextField();
        goldpanel = new JPanel();
        font = new Font("verdana", font.BOLD, 12);

        setOpaque(false);
        setSize(new java.awt.Dimension(250, 30));
        setLayout(new java.awt.BorderLayout());

        jProgressBar1.setSize(new java.awt.Dimension(116, 14));
        jProgressBar1.setFont(font);
        jProgressBar1.setString("Life");
        jProgressBar1.setStringPainted(true);
        jProgressBar1.setOpaque(false);
        add(jProgressBar1, java.awt.BorderLayout.CENTER);

        jLabel1.setSize(new java.awt.Dimension(14, 14));
        jLabel1.setOpaque(false);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/heart.png"))); // NOI18N
        jLabel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        add(jLabel1, java.awt.BorderLayout.LINE_START);
        
        goldlabel.setSize(new java.awt.Dimension(14, 14));
        goldlabel.setOpaque(false);
        goldlabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/gold.png")));
        goldlabel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        goldtext.setFont(font);
        goldtext.setText(" Gold: 200 ");
        goldtext.setEditable(false);
        goldtext.setSize(new java.awt.Dimension(40, 14));
        //goldtext.setOpaque(false);
        goldtext.setBorder(null);
        goldpanel.setLayout(new java.awt.BorderLayout());
        goldpanel.setSize(70,20);
        goldpanel.add(goldlabel, java.awt.BorderLayout.WEST);
        goldpanel.add(goldtext, java.awt.BorderLayout.EAST);
        goldpanel.setOpaque(false);
        add(goldpanel, java.awt.BorderLayout.EAST);
    }
    
    private void initBar(){
        model = new DefaultBoundedRangeModel(99,1,1,100);
        jProgressBar1.setModel(model);
        jProgressBar1.setForeground(Color.red);
        model.setValue(10);
        jProgressBar1.setString("Life: "+jProgressBar1.getModel().getValue());
        jProgressBar1.setUI(new BasicProgressBarUI() {
            protected Color getSelectionBackground() { return Color.black; }
            protected Color getSelectionForeground() { return Color.black; }
        });
    }
    
    public void setLife(int i){
        model.setValue(i);
        jProgressBar1.setString("Life: "+jProgressBar1.getModel().getValue());
    }
    
    public void setGold(int g){
        goldtext.setText(" Gold: "+g+" ");
    }
}
