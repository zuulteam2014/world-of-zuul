package controller;

import model.Player;

/**
 * This class is an abstract superclass for all command classes in the game.
 * Each user command is inherited from this abstract class.
 *
 * Objects of class Command can have an optional argument word (a second word
 * entered on the command line). If the command had only one word, the second
 * word is *null*.
 *
 * Objects of class Command can have Output (in response of the execution of a
 * command).
 */
public abstract class Command {

    private String secondWord;
    private String thirdWord;
    private String fourthWord;
    private String fifthWord;
    private String sixthWord;
    private String outputString;

    /*
     * Create a command object. First and second words must be supplied, but
     * either one (or both) can be null. The command word should be null to
     * indicate that this was a command that is not recognized by this game.
     */
    public Command() {
        secondWord = null;
        thirdWord = null;
        fourthWord = null;
        fifthWord = null;
        sixthWord = null;
        outputString = new String();
    }

    /*
     * Return the second word of this command. If no second word was entered,
     * the result is null.
     */
    public String getSecondWord() {
        return secondWord;
    }

    // Check whether a second word was entered for this command.
    public boolean hasSecondWord() {
        return secondWord != null;
    }

    /*
     * Define the second word of this command (the word entered after the
     * command word). Null indicates that there was no second word.
     */
    public void setSecondWord(String secondWord) {
        this.secondWord = secondWord;
    }

    public String getThirdWord() {
        return thirdWord;
    }

    public boolean hasThirdWord() {
        return thirdWord != null;
    }

    public void setThirdWord(String thirdWord) {
        this.thirdWord = thirdWord;
    }

    public String getFourthWord() {
        return fourthWord;
    }

    public boolean hasFourthWord() {
        return fourthWord != null;
    }

    public void setFourthWord(String fourthWord) {
        this.fourthWord = fourthWord;
    }

    public void setFifthWord(String fifthWord) {
        this.fifthWord = fifthWord;
    }

    public boolean hasFifthWord() {
        return fifthWord != null;
    }

    public String getFifthWord() {
        return fifthWord;
    }

    public void setSixthWord(String sixthWord) {
        this.sixthWord = sixthWord;
    }

    public boolean hasSixthWord() {
        return sixthWord != null;
    }

    public String getSixthWord() {
        return sixthWord;
    }

    // Clear the outputString
    public void clearOutputString() {
        outputString = "";
    }

    // Append String to the outputString
    public void appendToOutputString(String myString) {
        outputString += myString;
    }

    // Get field outputString
    public String getOutputString() {
        return outputString;
    }

    /*
     * Execute this command. A flag is returned indicating whether the game is
     * over as a result of this command.
     *
     * @return True, if game should exit; and false otherwise.
     */
    public abstract boolean execute(Player player);
}
