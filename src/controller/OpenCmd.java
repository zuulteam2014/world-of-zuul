/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Random;
import model.Item;
import model.ItemName;
import model.Player;
import model.Room;
import model.RoomName;

/**
 *
 * @author djayb6
 */
public class OpenCmd extends Command {

    @Override
    public boolean execute(Player player) {
    clearOutputString();
        switch (player.getCurrentRoom().getName()) {
            case RoomName.MAGIC_ROOM:

                if (hasSecondWord() && hasThirdWord() && getThirdWord().equals("door")) {
                    switch (getSecondWord()) {
                        case "red": // send you in a random room
                            
                            //  randomization (previous rooms)
                            
                            int maxRandomNumber = player.getPreviousRooms().size();
                            int index = new Random().nextInt(maxRandomNumber);
                            Room randomRoom = (Room)player.getPreviousRooms().get(index);
                            
                            
                            player.addPreviousRoom(player.getCurrentRoom());
                            player.setCurrentRoom(randomRoom);
                            
                            break;
                            
                        case "brown": // die
                        case "green": // die
                            
                            appendToOutputString("You've just opened the DEATH DOOR! Game Over.\n");
                            player.setLife(0);
                            
                            player.getCurrentRoom().setImageName("/images/gameover.png");
                            return true;

                        default:
                            appendToOutputString("Doors available are red, brown and yellow.\n");
                    }

                } else {

                    appendToOutputString("I don't understand what to open.\n");
                }

                break;

            case RoomName.STAIRS_ROOM:
                if (hasSecondWord() && hasThirdWord() && getThirdWord().equals("door")) {
                    if (getSecondWord().equals("throne")) {
                        if (player.getItemWithName(ItemName.KEY) != null) {
                            player.getCurrentRoom().getNeighborWithName(RoomName.THRONE_ROOM).unlock();
                            player.removeItem(player.getItemWithName(ItemName.KEY));

                            appendToOutputString("Throne room unlocked. Good luck!\n");

                        } else {
                            appendToOutputString("You need to open the throne door with a key.\n");
                        }
                    } else
                    {
                        appendToOutputString("Door available is throne door.\n");
                    }
                } else {
                    appendToOutputString("I don't understand what to open.\n");
                }
                break;

            default:
                appendToOutputString("This command has no effect here.\n");
                break;
        }

        return false;
    }

}
