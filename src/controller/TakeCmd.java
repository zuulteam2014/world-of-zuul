/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.CharacterName;
import model.Enemy;
import model.Item;
import model.ItemName;
import model.Player;
import model.Room;
import model.RoomName;
import model.Scores;

/**
 *
 * @author djayb6
 */
public class TakeCmd extends Command {

    @Override
    public boolean execute(Player player) {

        clearOutputString();
        if (hasSecondWord()) {
            String itemName = getSecondWord();
            Item item = player.getCurrentRoom().getItemWithName(itemName);

            if (item == null) {
                appendToOutputString(itemName + " not found!\n");
                return false;
            }

            if (!itemName.equals(ItemName.BAG) && player.getItemWithName(ItemName.BAG) == null) {
                appendToOutputString("You need a bag to carry further items.\n");
                return false;
            }
            
            if (item.getName().equals(ItemName.LANTERN) && player.hasMetFantom())
                item.setCanBeTaken(true);
            
            if (item.getName().equals(ItemName.FLUTE) && player.gotHintFromPrisoner())
                item.setCanBeTaken(true);
            
            if (!item.canBeTaken()) {
                appendToOutputString(itemName + " can't be taken for now.\n");
                return false;
            }

            if (!player.canAddItem(item)) {
                appendToOutputString(itemName + " is too heavy! You need to drop some items first.\n");
                return false;
            }
            
            if (itemName.equals(ItemName.CHEESE) 
                    || itemName.equals(ItemName.WINE) 
                    || itemName.equals(ItemName.BREAD))
            {
                Enemy fantom = (Enemy)player.getCurrentRoom().getCharacterWithName(CharacterName.FANTOM);
                
                // you can't get the food if the fantom is in the banquet hall
                
                if (player.getCurrentRoom().getName().equals(RoomName.BANQUET_HALL)
                        && fantom != null)
                {
                    appendToOutputString("The fantom attacked you while you were trying to get some food!\n");
                    player.decreaseLife(fantom.getAttackPower());
                    appendToOutputString("You lost " + fantom.getAttackPower() + " points of life during the attack.\n");
                    
                    if (player.isDead())
                    {
                        appendToOutputString("You are dead! GAME OVER.\n");
                        player.getCurrentRoom().setImageName("/images/gameover.png");
                        return true;
                    } else
                    {
                        appendToOutputString("You should try to make it flee with something!\n");
                        return false;
                    }
                }
            }
            if (!player.getCurrentRoom().getName().equals(RoomName.BANQUET_HALL))
                player.getCurrentRoom().removeItem(item);
            
            player.addItem(item);
            
            if (itemName.equals(ItemName.BAG)) {
            appendToOutputString(itemName + " added!\n");
            } else {
                appendToOutputString(itemName + " added to the bag!\n");
            }
            
            // WIN THE GAME
            
            if (itemName.equals(ItemName.CROWN)) {
                
                player.getCurrentRoom().setImageName("/images/youwin.png");
                
                player.setElapsedTime(System.nanoTime() - player.getStartTime() + player.getElapsedTime());
                
                Scores sc = new Scores();
                sc.addScore(player.getName(), player.getElapsedTime());
                
                //appendToOutputString(sc.getScores().toString());
                
                return true;
            }

        } else {
            appendToOutputString("Take what?\n");
        }

        return false;
    }

}
