/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import model.Character;
import model.Item;
import model.Player;
import model.Room;
import model.Scores;

/**
 *
 * @author djayb6
 */
public class ShowCmd extends Command {

    @Override
    public boolean execute(Player player) {
        clearOutputString();

        if (hasSecondWord()) {
            String what = getSecondWord();

            switch (what) {
                case "stats":
                    showStats(player);
                    break;
                case "characters":
                    showRoomCharacters(player);
                    break;
                case "items":
                    showRoomItems(player);
                    
                case "exits":
                    showExits(player);
                    break;
                    
                case "scores":
                    showScores();
                    break;
                    
                default:
                    appendToOutputString("I can't show that!\n");
                   
                    break;
            }

        } else {
            showExits(player);
            showStats(player);
            showRoomCharacters(player);
            showRoomItems(player);
            showScores();
            
        }

        return false;

    }
    
    private void showScores()
    {
        Scores sc = new Scores();
        HashMap<String, Long> theScores = sc.getScores();
        
        if (theScores != null)
        {
            appendToOutputString("Scores:\n");
            for (Entry<String, Long>entry : theScores.entrySet())
            {
                appendToOutputString(entry.getKey() + " - " + TimeUnit.SECONDS.convert(entry.getValue(), TimeUnit.NANOSECONDS) + "\n");
            }
        }
    }
    
    private void showStats(Player player)
    {
        appendToOutputString("Life: " + player.getLife() + " | Gold: " + player.getGold() + " | Weight: " +player.getCurrentWeight());
    }
    
    private void showRoomItems(Player player)
    {
        ArrayList<Item> items = player.getCurrentRoom().getItems();

                    appendToOutputString("Items:");

                    for (Item item : items) {
                        appendToOutputString(" " + item.getName() + " | ");
                    }
    }
    
    private void showPlayerItems(Player player)
    {
        
    }
    
    private void showRoomCharacters(Player player)
    {
        ArrayList<Character> characters = player.getCurrentRoom().getCharacters();
                    appendToOutputString("Characters:");

                    for (Character character : characters) {
                        appendToOutputString(" " + character.getName() + " | ");
                    }
                    appendToOutputString("\n");
    }
    
    private void showExits(Player player)
    {
        HashMap<String, Room> exits = player.getCurrentRoom().getExits();
                    appendToOutputString("Exits:");
                    for (Entry<String, Room> entry : exits.entrySet())
                    {
                        appendToOutputString(" " + entry.getValue().getName() + " [" + entry.getKey()+ "]" + " | ");
                    }

                    appendToOutputString("\n");
    }

}
