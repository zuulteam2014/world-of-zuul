/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Player;

/**
 *
 * @author djayb6
 */
public class SaveCmd extends Command {

    @Override
    public boolean execute(Player player) {

        clearOutputString();
        
        player.setElapsedTime(System.nanoTime() - player.getStartTime() + player.getElapsedTime());
        
        String folderString = System.getProperty("user.home") + File.separator + "zuul";
        String saveString = folderString + File.separator + player.getName() + ".save";
        new File(folderString).mkdirs();

        try {

            FileOutputStream fos = new FileOutputStream(saveString);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(player);

            oos.close();
            fos.close();

            appendToOutputString("Game saved to " + saveString + "\n");
        } catch (IOException ex) {
            Logger.getLogger(SaveCmd.class.getName()).log(Level.SEVERE, null, ex);

            appendToOutputString("Failed to save game");
        }

        return false;
    }

}
