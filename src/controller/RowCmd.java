/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Player;
import model.RoomName;

/**
 *
 * @author djayb6
 */
public class RowCmd extends Command {

    private static int rowingNumber = 5;

    @Override
    public boolean execute(Player player) {
    clearOutputString();
        if (player.getCurrentRoom().getName().equals(RoomName.RIVER)) {
            
            rowingNumber--;
            
            if (rowingNumber == 0) {
                player.getCurrentRoom().unlock();
                player.getCurrentRoom().getNeighborWithName(RoomName.CASTLE_SHORE).unlock();

                appendToOutputString("You've just arrived at destination. You can continue towards the castle shores\n");
            } else if (rowingNumber > 0) {
                player.getCurrentRoom().lock();

                appendToOutputString("You need to row " + rowingNumber + " more times");
            }

        } else {
            appendToOutputString("You can't row here!\n");
        }

        return false;

    }

}
