/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Item;
import model.ItemName;
import model.LifeItem;
import model.Player;

/**
 *
 * @author djayb6
 */
public class DrinkCmd extends Command {

    @Override
    public boolean execute(Player player) {
        
        clearOutputString();
        
        if (hasSecondWord()) {
            Item item = player.getItemWithName(getSecondWord());

            if (item != null) {
                switch (item.getName()) {
                    case ItemName.BEER:
                    case ItemName.POTION:
                    case ItemName.GOURD:
                    case ItemName.WINE:
                        LifeItem lifeItem = (LifeItem) item;
                        player.increaseLife(lifeItem.getLifeGain());
                        player.removeItem(lifeItem);

                        appendToOutputString("You drank some " + lifeItem.getName() + ".\n"
                                + "You gained " + lifeItem.getLifeGain() + " points of life.\n");
                        break;

                    default:
                        appendToOutputString("You can't drink that!\n");
                        break;
                }
            } else {
                appendToOutputString("You don't have this item in your bag.\n");
            }
        } else {

            appendToOutputString("What do you want to drink?\n");

        }

        return false;
    }

}
