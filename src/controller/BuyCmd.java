/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Character;
import model.Item;
import model.Player;

/**
 *
 * @author djayb6
 */
public class BuyCmd extends Command {

    @Override
    public boolean execute(Player player) {

        clearOutputString();

        if (hasSecondWord() && hasThirdWord() && hasFourthWord() && getThirdWord().equals("from")) {
            String characterName = getFourthWord();
            Character character = player.getCurrentRoom().getCharacterWithName(characterName);

            if (character == null) {
                appendToOutputString("There is no such character in that room?\n");
                return false;
            }

            if (!character.isMerchant()) {
                appendToOutputString("This character is not a merchant\n");
                return false;
            }

            String itemName = getSecondWord();
            Item item = character.getItemWithName(itemName);

            if (item == null) {
                appendToOutputString(characterName + " doesn't sell this\n");
                return false;
            }

            if (!player.canAddItem(item)) {
                appendToOutputString(itemName + " is too heavy. Please drop something\n");
                return false;
            }

            // if player has enough money
            if (player.getGold() >= item.getPrice()) {
                player.addItem(item);
                player.setGold(player.getGold() - item.getPrice());

                appendToOutputString("You've just bought " + itemName + "\n");

            } else {
                appendToOutputString("You don't have enough money\n");
            }
        }

        return false;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
