/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Item;
import model.ItemName;
import model.Player;
import model.RoomName;

/**
 *
 * @author djayb6
 */
public class BuildCmd extends Command {

    @Override
    public boolean execute(Player player) {
        clearOutputString();

        if (hasSecondWord() && hasThirdWord() && hasFourthWord() && getThirdWord().equals("with")) {
            String object = getSecondWord();

            if (object.equals(ItemName.RAFT)) // raft needs wood and string
            {
                if (!hasFifthWord()) {
                    appendToOutputString("You need two materials to build the raft.\n");
                    return false;
                }

                String material1 = getFourthWord();
                String material2 = getFifthWord();
                
           

                if (!(material1.equals(ItemName.WOOD) && material2.equals(ItemName.ROPE)
                        || material1.equals(ItemName.ROPE) && material2.equals(ItemName.WOOD))) {
                    appendToOutputString("You can't build a raft with these materials!\n");
                    return false;
                }

                Item wood = player.getItemWithName(ItemName.WOOD);
                Item rope = player.getItemWithName(ItemName.ROPE);

                
                
                if (wood != null && rope != null) {
                    if (player.getCurrentRoom().getName().equals(RoomName.WATER_MILL)
                            || player.getCurrentRoom().getName().equals(RoomName.FOREST)) {

                        player.removeItem(wood);
                        player.removeItem(rope);

                        Item raft = new Item(ItemName.RAFT, "a solid raft", 11000, -1, true, null, null);
                        player.getCurrentRoom().addItem(raft);

                        player.getCurrentRoom().getNeighborWithName(RoomName.PONTOON).unlock();

                    } else {
                        appendToOutputString("You have to be in the forest or in the watermill to build the raft.\n");
                    }

                } else {

                    appendToOutputString("You don't have the necessary materials in your inventory!\n");

                }

            } else {
                
                appendToOutputString("You can't build a " + object);
            }
        } else {
            
            appendToOutputString("Usage: build item with item1 item2");
        }

        return false;
    }

}
