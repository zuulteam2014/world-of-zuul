/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.Player;
import model.Character;
import model.CharacterName;
import model.ItemName;

/**
 *
 * @author djayb6
 */
public class TalkCmd extends Command {

    @Override
    public boolean execute(Player player) {

        clearOutputString();

        if (hasSecondWord() && hasThirdWord() && getSecondWord().equals("to")) {

            String characterName = getThirdWord();
            Character character = player.getCurrentRoom().getCharacterWithName(characterName);

            if (character != null) {
                
                switch(character.getName())
                {
                    case CharacterName.PRISONER:
                        appendToOutputString("Prisoner: Please give some food, and in return I will give you a good hint about the gatekeeper.\n");
                        break;
                        
                    case CharacterName.BARTENDER:
                        appendToOutputString("Bartender: Hello! I'm selling some beer for " 
                                + character.getItemWithName(ItemName.BEER).getPrice()
                        + " and some potion for " + character.getItemWithName(ItemName.POTION).getPrice() +"\n");
                        break;
                        
                    case CharacterName.GANDALF:
                        appendToOutputString("Gandalf The Purple: Long live to the king!\n");
                }
                
                //appendToOutputString(character.getDescription() + "\n");

            } else {

                appendToOutputString("There is no character named " + characterName + " in this room\n");
            }

        } else {

            appendToOutputString("Second and/or third argument missing!\n");
            appendToOutputString("Usage: talk to character\n");

        }
        return false;
    }

}
