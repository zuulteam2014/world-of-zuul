/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.CharacterName;
import model.Friend;
import model.ItemName;
import model.Player;
import model.RoomName;

/**
 *
 * @author djayb6
 */
public class GiveCmd extends Command {

    @Override
    public boolean execute(Player player) {

        clearOutputString();
        if (hasSecondWord() && hasThirdWord() && hasFourthWord() && getThirdWord().contains("to")) {
            String itemName = getSecondWord();
            String characterName = getFourthWord();

            if (characterName.equals(CharacterName.PRISONER) && player.getCurrentRoom().getName().equals(RoomName.PRISON)) {
                if (itemName.equals(ItemName.BREAD) || itemName.equals(ItemName.CHEESE) || itemName.equals(ItemName.WINE)) {
                    if (player.getItemWithName(itemName) != null) {
                        Friend prisoner = (Friend) player.getCurrentRoom().getCharacterWithName(CharacterName.PRISONER);
                        
                        if (prisoner.getItems().size() >= 2) {
                                appendToOutputString("Prisoner: Thanks for your generosity my king. If you want to get the gate, you must put to sleep the gate keeper with a flute.\n");
                                player.setGotHintFromPrisoner(true);
                                
                                return false;
                            }
                        
                        
                        if (prisoner.getItemWithName(itemName) != null) {
                            appendToOutputString("Prisoner: You already gave me that. Give me something else. I am STARVING!\n");
                        } else {
                            
                            prisoner.addItem(player.getItemWithName(itemName));
                            
                            appendToOutputString("Prisoner: I love " + itemName + ". Thank you!");
                            
                        }

                    } else {
                        appendToOutputString("You don't have this in your bag.\n");
                    }

                } else {
                    appendToOutputString("Prisoner: I don't want that!\n");
                }

            } else {
                appendToOutputString("This command has no effect here.\n");
            }

        } else {
            appendToOutputString("Bad arguments.\nUsage: give item to character\n");
        }

        return false;
    }

}
