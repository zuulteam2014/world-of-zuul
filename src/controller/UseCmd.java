/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.CharacterName;
import model.Item;
import model.ItemName;
import model.Player;
import model.Room;
import model.RoomName;

/**
 *
 * @author djayb6
 */
public class UseCmd extends Command {

    @Override
    public boolean execute(Player player) {
        clearOutputString();

        if (hasSecondWord()) {
            String itemName = getSecondWord();
            Item item = player.getItemWithName(itemName);
            Room currentRoom = player.getCurrentRoom();

            if (item != null) {
                switch (item.getName()) {

                    case ItemName.RAFT:
                        if (currentRoom.getName().equals(RoomName.PONTOON)) {
                            if (player.getLife() < 100) {
                                appendToOutputString("You need all your life to navigate with the raft.\n");
                                return false;
                            } else {
                                player.getCurrentRoom().getNeighborWithName(RoomName.RIVER).unlock();
                                appendToOutputString("You can going through the river!\n");
                                return false;
                            }
                        }
                        break;

                    case ItemName.KEY:
                        if (currentRoom.getName().equals(RoomName.STAIRS_ROOM)) {
                            player.getCurrentRoom().getNeighborWithName(RoomName.THRONE_ROOM).unlock();
                            appendToOutputString("Throne room unlocked. Good luck!\n");
                            return false;
                        }
                        break;
                        
                    case ItemName.LANTERN:
                       if (currentRoom.getName().equals(RoomName.BANQUET_HALL)) {
                           player.getCurrentRoom().setImageName("/images/banquet_hall_no_fantom.jpg");
                           currentRoom.removeCharacter(currentRoom.getCharacterWithName(CharacterName.FANTOM));
                           player.removeItem(player.getItemWithName(ItemName.LANTERN));
                           
                           appendToOutputString("It made the fantom get away!\n");
                           
                           return false;
                       }
                    
                    case ItemName.BEARSKIN:
                        if (currentRoom.getName().equals(RoomName.LOBBY)) {
                           
                           player.getCurrentRoom().getNeighborWithName(RoomName.BANQUET_HALL).unlock();
                           player.removeItem(player.getItemWithName(ItemName.BEARSKIN));
                           
                            appendToOutputString("You've just walk through the guards by wearing this magical bearskin!\n");
                           return false;
                       }
                    
                        
                    
                   
                }

                appendToOutputString("This has no effects here.\n");

            } else {
                appendToOutputString("You don't have such item in your bag.\n");
            }

        } else {

            appendToOutputString("What do you want to use?\n");
        }

        return false;
    }

}
