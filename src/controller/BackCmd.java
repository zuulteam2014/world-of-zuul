package controller;

import java.util.HashMap;
import java.util.Map;
import model.Player;
import model.Room;

/**
 * Implementation of *Back* command.
 *
 */
public class BackCmd extends Command {

    public BackCmd() {
    }

    /*
     * Try to go to previous room if there is one. Otherwise print an error
     * message. Returns always 'false'.
     */
    public boolean execute(Player player) {
        
        clearOutputString();
        Room lastRoom = player.getLastRoom();

        if (lastRoom == null) {
            appendToOutputString("There is no door!");
        } else {
            player.addPreviousRoom(player.getCurrentRoom());
            player.setCurrentRoom(lastRoom);
            appendToOutputString("You are " + lastRoom.getDescription() + ".\n");
            int i = 0;
            //Waits for a while
            while (i < 100000) {
                i++;
            }
            appendToOutputString("Exits:" + lastRoom.getExits().keySet());
        }

        return false;
    }
}
