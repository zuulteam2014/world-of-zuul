/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Item;
import model.ItemName;
import model.Player;
import model.Room;
import model.RoomName;

/**
 *
 * @author djayb6
 */
public class ThrowCmd extends Command {

    @Override
    public boolean execute(Player player) {

        clearOutputString();

        if (hasSecondWord()) {
            String itemName = getSecondWord();
            Item item = player.getItemWithName(itemName);
            Room currentRoom = player.getCurrentRoom();

            if (item != null) {
                if (item.getName().equals(ItemName.ROCK)) {
                    if (currentRoom.getName().equals(RoomName.HUT_YARD)) {
                        player.getCurrentRoom().getNeighborWithName(RoomName.HUT).unlock();
                        player.removeItem(item);
                        appendToOutputString("You broke the hut door with the rock!\n");

                    } else {
                        appendToOutputString("Throwing the rock had no effect.\n");
                    }
                } else {
                    appendToOutputString("You can't throw this item.\n");
                }
            } else {
                appendToOutputString("There is no " + itemName + " in your bag.\n");
            }

        } else {
            appendToOutputString("What do you want to throw?\n");
        }
        return false;
    }

}
