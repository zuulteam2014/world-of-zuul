package controller;

import java.awt.Point;
import java.util.Random;
import model.CharacterName;
import model.Enemy;
import model.Friend;
import model.ItemName;
import model.Player;
import model.Room;
import model.RoomName;

/**
 * Implementation of *Go* command.
 */
public class GoCmd extends Command {

    public GoCmd() {
    }

    /*
     * Goes to the direction which is entered as the second word. If there is no
     * exit for that direction, prints an error message. Returns always *false*.
     */
    public boolean execute(Player player) {
        clearOutputString();

        if (player.getCurrentRoom().isLocked()) {
            if (player.getCurrentRoom().getName().equals(RoomName.RIVER)) {
                appendToOutputString("You have no choice but rowing!\n");

            } else {

                appendToOutputString("You can't run away from a fight!\n");
            }
            return false;
        }

        if (hasSecondWord()) {
            String direction = getSecondWord();
            Room nextRoom = player.getCurrentRoom().getExit(direction);

            if (nextRoom == null) {
                appendToOutputString("There is no door!");
            } else {

                if (nextRoom.isLocked()) {

                    switch (nextRoom.getName()) {
                        case RoomName.HUT:
                            if (player.getItemWithName(ItemName.ROCK) != null) {
                                appendToOutputString("Use the rock to break the hut door!\n");
                                return false;

                            } else {
                                appendToOutputString("The door is closed, but seems fragile...\n");
                                return false;
                            }

                        case RoomName.PONTOON:
                            appendToOutputString("You need to build a raft to get into this room.\n");
                            return false;

                        case RoomName.RIVER:
                            appendToOutputString("You need to use the raft first!.\n");
                            return false;
                            
                        case RoomName.CASTLE_SHORE:
                            appendToOutputString("You have no choice but rowing!\n");
                            return false;

                        case RoomName.BANQUET_HALL:
                            appendToOutputString("You need to hide yourself from the guards with something!\n");
                            return false;

                        case RoomName.PRISON:
                            Enemy aligator = (Enemy) player.getCurrentRoom().getCharacterWithName(CharacterName.ALIGATOR);
                            if (!aligator.isDead()) {
                                appendToOutputString("The aligator is blocking the passage! Why not kill it?\n");
                                return false;
                            }

                        case RoomName.THRONE_ROOM:
                            appendToOutputString("You need a key to open the throne room!\n");
                            return false;

                    }
                }
                
                if (player.getCurrentRoom().getName().equals(RoomName.BANQUET_HALL))
                    player.setHasMetFantom(true);
                
                player.addPreviousRoom(player.getCurrentRoom());
                player.setCurrentRoom(nextRoom);
                
                // if gandalf was in the last room, we remove it else he will be twice or more in the same room
                if (player.getLastRoom().getCharacterWithName(CharacterName.GANDALF) != null)
                    player.getLastRoom().removeCharacter(player.getLastRoom().getCharacterWithName(CharacterName.GANDALF));
                
                // we want gandalf to appear only after visiting 24 rooms
                if (player.getPreviousRooms().size() > 23)
                {
                int number = new Random().nextInt(20);
                
                if ((number % 6) == 0) // less occurences than nextBoolean
                {
                    Friend gandalf = new Friend(CharacterName.GANDALF, "the reknown Gandalf the Purple", "/images/gandalf.png", new Point(300, 300), 10000);
                    player.getCurrentRoom().addCharacter(gandalf);
                    appendToOutputString("Gandalf The Purple just appeared!\n");
                }
                }
                appendToOutputString("You are in " + nextRoom.getDescription() + ".\n");
                int i = 0;
                //Waits for a while
                while (i < 100000) {
                    i++;
                }
                appendToOutputString("Exits:" + nextRoom.getExits().keySet());
            }

        } else {
            // if there is no second word, we don't know where to go...
            appendToOutputString("Go where?");
        }
        return false;
    }
}
