/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Game;
import model.Player;

/**
 *
 * @author djayb6
 */
public class LoadCmd extends Command {

    @Override
    public boolean execute(Player player) {

        clearOutputString();

        try {
            
           String savePath;
            
           if(hasSecondWord()) savePath =  System.getProperty("user.home") + File.separator + "zuul" +  File.separator + getSecondWord() + ".save";
           else savePath =  System.getProperty("user.home") + File.separator + "zuul" +  File.separator + player.getName() + ".save"; 
               
           FileInputStream fis =  new FileInputStream(savePath);
        
           ObjectInputStream ois =  new ObjectInputStream(fis);
           
           Player p = (Player)ois.readObject();
           
           ois.close();
           fis.close();
           
           player.setName(p.getName());
           player.setLife(p.getLife());
           player.setGold(p.getGold());
           player.setCurrentRoom(p.getCurrentRoom());
           player.setPreviousRooms(p.getPreviousRooms());
           player.setItems(p.getItems());
           player.setElapsedTime(p.getElapsedTime());
           player.setStartTime(System.nanoTime());
           
            appendToOutputString("Game loaded.\n");

            //System.out.println("Gold: " + p.getGold());
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            appendToOutputString("Failed to load the game!\n");
        }

        return false;
    }

}
