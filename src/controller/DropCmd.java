/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Item;
import model.ItemName;
import model.Player;

/**
 *
 * @author djayb6
 */
public class DropCmd extends Command {

    @Override
    public boolean execute(Player player) {
        clearOutputString();
        if (hasSecondWord()) {
            String itemName = getSecondWord();
            Item item = player.getItemWithName(itemName);

            if (item != null) {

                if (itemName.equals(ItemName.BAG)) {
                    appendToOutputString("You can't drop the your bag!\n");
                    return false;
                }

                player.removeItem(item);
                player.getCurrentRoom().addItem(item);
                appendToOutputString(itemName + " dropped in " + player.getCurrentRoom().getName() + "\n");

            } else {

                appendToOutputString(itemName + " not found in the bag.\n");
            }

        } else {
            appendToOutputString("Drop what?\n");
        }

        return false;
    }
}
