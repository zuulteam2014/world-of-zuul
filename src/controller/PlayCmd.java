/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Item;
import model.ItemName;
import model.Player;
import model.RoomName;

/**
 *
 * @author djayb6
 */
public class PlayCmd extends Command {

    @Override
    public boolean execute(Player player) {
        clearOutputString();
        if (hasSecondWord()) {
            if (getSecondWord().equals(ItemName.FLUTE)) {
                if (player.getItemWithName(ItemName.FLUTE) != null) {

                    if (player.getCurrentRoom().getName().equals(RoomName.OFFICE)) {
                        player.getCurrentRoom().setImageName("/images/office_no_gatekeeper.jpg");
                        appendToOutputString("The Gate Keeper is falling asleep...\nThe gate is free!\n");
                        
                        Item key = player.getCurrentRoom().getItemWithName(ItemName.KEY);
                        
                        if (key != null)
                            key.setCanBeTaken(true);
                        
                        
                    } else {
                        appendToOutputString("Nothing happens. Play it elsewhere\n");
                    }

                } else {
                    appendToOutputString("You don't have the flute in your bag\n");
                }

            } else {
                appendToOutputString("I can't play that.\n");
            }
        }

        return false;
    }

}
