/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Character;
import model.CharacterName;
import model.Enemy;
import model.ItemName;
import model.Player;
import model.RoomName;
import model.Weapon;

/**
 *
 * @author djayb6
 */
public class FightCmd extends Command {

    @Override
    public boolean execute(Player player) {

        clearOutputString();
        
        if (hasSecondWord()) //  character
        {
            String enemyName, weaponName;
            Weapon weapon;
            Character character;
            int attackPower;

            attackPower = player.getAttackPower(); // attackPower of fists' player
            enemyName = getSecondWord();
            character = player.getCurrentRoom().getCharacterWithName(enemyName);
            weapon = null;
            weaponName = null;

            // if player selects a weapon with `with` keyword
            if (hasThirdWord() && hasFourthWord()
                    && getThirdWord().equals("with")) {
                weaponName = getFourthWord();
                weapon = player.getWeaponWithName(weaponName);

                if (weapon != null) {
                    attackPower = weapon.getPowerAttack();
                } else
                {
                    appendToOutputString("There is no such weapon in the bag!\n");
                }
            }

            if (character != null
                    && character.isEnemy()) {

                Enemy enemy = (Enemy) character;

                switch (enemy.getName()) {
                    case CharacterName.FANTOM:
                        appendToOutputString("You can't fight a ghost! You need to find something to make him get away!\n");
                        return false;

                    case CharacterName.GATE_KEEPER:
                        appendToOutputString("The gatekeeper is immortal! You need to find another way to get the gate's content.\n");
                        return false;

                    case CharacterName.ALIGATOR:
                        if (weapon == null || !weapon.getName().equals(ItemName.ARBALET)) {
                            appendToOutputString("The aligator is too far for this weapon!\n");
                            return false;
                        }
                        break;
                    case CharacterName.DRAGON:
                        if (weapon == null || !weapon.getName().equals(ItemName.SWORD)) {
                            appendToOutputString("You need to fight with the most powerful weapon!\n");
                            return false;
                        }
                        break;

                }

                /*
                 if (enemy.isDead())
                 {
                 appendToOutputString("You can't fight something already dead\n");
                 return false;
                 }
                 */
                // lock the room so the player can't leave until he killed the enemy
                player.getCurrentRoom().lock();

                appendToOutputString(enemyName + " lost " + attackPower + " points of life\n");
                enemy.decreaseLife(attackPower);

                if (enemy.isDead()) {

                    player.getCurrentRoom().removeCharacter(enemy);

                    switch (enemy.getName()) {

                        case CharacterName.BEAR:
                            player.getCurrentRoom().setImageName("/images/mountain_no_bear.jpg");
                            enemy.getItemWithName(ItemName.BEARSKIN).setCanBeTaken(true);
                            player.getCurrentRoom().getItemWithName(ItemName.FRUIT).setCanBeTaken(true);
                            player.getCurrentRoom().addItem(enemy.getItemWithName(ItemName.BEARSKIN));
                            break;

                        case CharacterName.BLACK_KNIGHT:
                            player.getCurrentRoom().setImageName("/images/watermill_no_knight.jpg");
                            player.getCurrentRoom().getItemWithName(ItemName.WOOD).setCanBeTaken(true);
                            //player.getCurrentRoom().addItem(enemy.getItemWithName(ItemName.WOOD));
                            break;

                        case CharacterName.TARENTULA:
                            player.getCurrentRoom().setImageName("/images/armory_no_tarentula.jpg");
                            player.getCurrentRoom().getItemWithName(ItemName.SWORD).setCanBeTaken(true);
                            player.getCurrentRoom().getItemWithName(ItemName.ARBALET).setCanBeTaken(true);
                            break;

                        case CharacterName.DRAGON:
                            player.getCurrentRoom().setImageName("/images/throne_room_no_dragon.jpg");
                            enemy.getItemWithName(ItemName.CROWN).setCanBeTaken(true);
                            player.getCurrentRoom().addItem(enemy.getItemWithName(ItemName.CROWN));
                            break;

                        case CharacterName.ALIGATOR:
                            player.getCurrentRoom().setImageName("/images/sewers_no_aligator.jpg");
                            player.getCurrentRoom().getNeighborWithName(RoomName.PRISON).unlock();
                            break;

                        default:
                            break;

                    }

                    player.increaseGold(enemy.getGold());

                    player.getCurrentRoom().unlock();
                    appendToOutputString("The " + enemyName + " is dead! You earned " + enemy.getGold() + " in gold\n");

                } else {
                    // enemy fights back!

                    player.decreaseLife(enemy.getAttackPower());
                    appendToOutputString("The " + enemyName + " fights back! Yous lost " + enemy.getAttackPower() + " points of life\n");

                }

                if (player.isDead()) {
                    appendToOutputString("You are dead. GAME OVER.\n");
                    player.getCurrentRoom().setImageName("/images/gameover.png");
                    return true;
                }

            } else if (character == null){

                appendToOutputString(enemyName + " doesn't exist\n");
            } else 
            {
                appendToOutputString("This is not an enemy.\n");
            }

        } else {

            appendToOutputString("Who do you want to fight?");
        }

        return false;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
